
var options = {
    settings: {
        duration: 2000
    }
};

function error_msg(data)
        {
         
            var i = 0;
            {       // console.log(data);
            for (var key in data)
                if (i == 0)
                {
                    if(data[key].length){
                        iqwerty.toast.Toast(data[key],options);
                    }
                   
                }
                i++;
            }
        }
$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );  
     
});

$( function() {
    $('#deliverydate').datepicker({
        format: "dd-mm-yyyy",
        startDate: "today",   
        toggleActive: true,
        keyboardNavigation: false,
        autoclose: true,
        todayHighlight: true,        
    });
});
    


$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $(".default_order_img_upload").show();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".default_order_img_upload").hide();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});

//second image upload
$(document).on('click', '#close-preview', function(){ 
   
     $('.second-image-preview').popover('hide');
    // Hover befor close the preview
    $('.second-image-preview').hover(
        function () {
           $('.second-image-preview').popover('show');
        },      
         function () {
           $('.second-image-preview').popover('hide');
        }
    );  
     
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.second-image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.second-image-preview-clear').click(function(){
        $('.second-image-preview').attr("data-content","").popover('hide');
        $('.second-image-preview-filename').val("");
        $('.second-image-preview-clear').hide();
        $('.second_default_order_img_upload').show();
        $('.second-image-preview-input:file').val("");
        $(".second-image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".second-image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".second-image-preview-input-title").text("Change");
            $(".second-image-preview-clear").show();
            $(".second_default_order_img_upload").hide();
            $(".second-image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".second-image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});

//third image upload

$(document).on('click', '#close-preview', function(){ 
   
     $('.third-image-preview').popover('hide');
    // Hover befor close the preview
    $('.third-image-preview').hover(
        function () {
           $('.third-image-preview').popover('show');
        }, 
         function () {
           $('.third-image-preview').popover('hide');
        }
    );  
     
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.third-image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.third-image-preview-clear').click(function(){
        $('.third-image-preview').attr("data-content","").popover('hide');
        $('.third-image-preview-filename').val("");
        $('.third-image-preview-clear').hide();
        $('.third_default_order_img_upload').show();
        $('.third-image-preview-input input:file').val("");
        $(".third-image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".third-image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".third-image-preview-input-title").text("Change");
            $(".third-image-preview-clear").show();
            $(".third_default_order_img_upload").hide();
            $(".third-image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".third-image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});

//upload profile img

$(document).on('click', '#close-preview', function(){ 
   
     $('.upload-image-preview').popover('hide');
    // Hover befor close the preview
    $('.upload-image-preview').hover(
        function () {
           $('.upload-image-preview').popover('show');
        }, 
         function () {
           $('.upload-image-preview').popover('hide');
        }
    );  
     
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.upload-image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.upload-image-preview-clear').click(function(){
        $('.upload-image-preview').attr("data-content","").popover('hide');
        $('.upload-image-preview-filename').val("");
        $('.upload-image-preview-clear').hide();
        $('.upload-image-preview-input input:file').val("");
        $(".upload-image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".upload-image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".upload-image-preview-input-title").text("Change");
            $(".upload-image-preview-clear").show();
            $(".upload-image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".upload-image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  

});


        
function check_user_profile() {
    
            var username=$("#username").val();
            var password=$("#password").val();
            
            if(username==""){
                iqwerty.toast.Toast('User name required!');
            }else if(password==""){
                iqwerty.toast.Toast('Password name required!');
            }

        $.ajax({
            url: url + "check_user",
            type: "POST",
            dataType: "json",
            data: $('form#user_check').serialize(),
             success: function (data, textStatus, jqXHR)
            { console.log(data);
              if (data == "success") {
                  
                    setTimeout(function () {
                        location.reload();

                    }, 500)
                } else {
                  error_msg(data.error);
                    
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
}

function customer_order(action) {
        var order_name=$("#order_name").val();
        var datepicker=$("#deliverydate").val();
        var quantity=$("#quantity").val();
        var weight_range_id=$("#weight_range_id").val();
         $('#creatOrder_btn').attr('disabled', true);
        if(order_name==""){
             $('#creatOrder_btn').attr('disabled', false);
            iqwerty.toast.Toast('Please Enter Order Name',options);
            return false;
        }else if(datepicker==""){
             $('#creatOrder_btn').attr('disabled', false);
            iqwerty.toast.Toast('Please Select Delivery Date',options);
            return false;
        }else if(quantity==""){
             $('#creatOrder_btn').attr('disabled', false);
            iqwerty.toast.Toast('Please Enter Quantity',options);
            return false;
        }else if(weight_range_id==""){
             $('#creatOrder_btn').attr('disabled', false);
            iqwerty.toast.Toast('Please Enter Weight',options);
            return false;
        }
        $('#creatOrder_btn').attr('disabled', true);
 	var formData = new FormData(document.querySelector("form#customer_order_form"));
 if($('input[type=file]')[0].files[0]){

      formData.append("image[0]", $('input[type=file]')[0].files[0]);
   } 
     if($('input[type=file]')[1].files[0]){
      formData.append("image[1]", $('input[type=file]')[1].files[0]);
   } 
     if($('input[type=file]')[2].files[0]){
      formData.append("image[2]", $('input[type=file]')[2].files[0]);
   } 
  //console.log(formData);    
//console.log(formData);
    $.ajax({
        url: url + "Create_order/" + action,
        type: "POST",
	    enctype: 'multipart/form-data',
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
    	cache: false,
        success: function (data, textStatus, jqXHR)
        {
                //console.log(data);
            if (data.status == 'failure') {
      $('#creatOrder_btn').attr('disabled', false);
                error_msg(data.error);

            }else if (data.status == "success") {
                   $('#creatOrder_btn').attr('disabled', true);
                if (data.order_status == 0) {                    
                    if (action == 'store') {
                        msg = "Successfully Saved";
                    } else {
                        msg = "Update Successfully";
                    }

                }
               var redirect_url = url + 'User_dashboard';
                    
                    iqwerty.toast.Toast(msg,options);
               
                    setTimeout(function () {
                        window.location.href = redirect_url;
                    }, 500)
            }else{
               $('#creatOrder_btn').attr('disabled', false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    window.location.href
                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
  


function cancel_order(id) {
        $(".alertBox").show();
        $(".alertBox .btntext").on("click", function(){
            if($(this).attr("id") =="No"){
                $(this).parents(".alertBox").hide();
                return false;
            }else{
                $.ajax({
                    url: url + "change_status",
                    type: "POST",
                    dataType: "json",
                    data: {id:id},
                     success: function (data, textStatus, jqXHR)
                    {   //console.log(data.error);
                      if (data.status == "success") {
                           iqwerty.toast.Toast('Your order cancelled successfully',options);
                            setTimeout(function () {
                                window.location.href = url+'order_details/'+id;
                            }, 500)
                        } else {
                          error_msg(data.error);
                            
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });

            }
        });
      
}

function update_password_data(){
    var old_password=$("#old_password").val();
    var new_password=$("#new_password").val();
    var confirm_password=$("#confirm_password").val();
    if(old_password==""){
        iqwerty.toast.Toast('Please Enter Current Password',options);
        return false;
    }else if(new_password==""){
        iqwerty.toast.Toast('Please Enter New Password',options);
        return false;
    }else if(confirm_password==""){
        iqwerty.toast.Toast('Please Enter Confirm Password',options);
        return false;
    }else if(confirm_password!=new_password){
        iqwerty.toast.Toast('Confirm Password should be same to password',options);
        return false;
    }
  


       $.ajax({
        url: url + "change_password",
        type: "POST",
        dataType: "json",
        data: $('form#update_password').serialize(),
         success: function (data, textStatus, jqXHR)
        {   console.log(data.error);
          if (data.status == "success") {
               iqwerty.toast.Toast('Password Successfully changed',options);
                setTimeout(function () {
                    window.location.href = url+'User/logout';
                }, 500)
            } else {
                iqwerty.toast.Toast('Current Password Not Match',options);
                
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

}


  $(document).ready(function() {
    $(".fancybox").fancybox({
      loop     : true,
      toolbar   : false,
    });    
  });


    $(document).ready(function(){
        $(".linkRegister").click(function(){
            $( ".frmRg" ).show();
            $(".frmLogin").hide();

        });
        $(".linkLogin").click(function(){
            $( ".frmLogin" ).show();
            $(".frmRg").hide();

        });
        $(".linkforgotPass").click(function(){
            $( ".frmforgotPass" ).show();
            $(".frmLogin").hide();

        });

    });

    function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };

 function create_user() {
    
            var name=$("#name").val();
            var user_email=$("#user_email").val();
            var user_mobile=$("#user_mobile").val();
            var user_company=$("#user_company").val();
            var user_password=$("#user_password").val();
            var user_company=$("#user_company").val();
            var user_conf_password=$("#user_conf_password").val();
            
            if(name==""){
                iqwerty.toast.Toast('Please Enter Name',options);
                    
            }else if(user_email==""){
                iqwerty.toast.Toast('Please Enter Valid Email Id',options);
                return false;
            }else if(user_email==""){
                iqwerty.toast.Toast('Please Enter Valid Email Id',options);
                return false;
            }else if(!ValidateEmail(user_email)){
                iqwerty.toast.Toast('Please Enter Valid Mobile',options);
                return false;
            }else if(user_mobile.length!=10){
                iqwerty.toast.Toast('Please Enter Valid Mobile',options);
                return false;
            }else if(user_company==""){
                iqwerty.toast.Toast('Please Enter Company',options);
                return false;
            }else if(user_password==""){
                iqwerty.toast.Toast('Please Enter Password',options);
                return false;
            }else if(user_conf_password==""){
                iqwerty.toast.Toast('Please Enter Confirm Password',options);
                return false;
            }else if(user_conf_password!=user_password){
                iqwerty.toast.Toast('Confirm Password should be equal to password',options);
                return false;
            }
                
        $.ajax({
            url: url + "check_user_profile",
            type: "POST",
            dataType: "json",
            data: $('form#create_user_profile').serialize(),
             success: function (data, textStatus, jqXHR)
            {   //console.log(data.error);
              if (data.status == "success") {
                   iqwerty.toast.Toast('Successfully Registration',options);
                    setTimeout(function () {
                        window.location.href = url+'User';
                    }, 500)
                } else {
                  error_msg(data.error);
                    
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
}

function check_user_profile() {
        
            var username=$("#username").val();
            var password=$("#password").val();
            
            if(username==""){
                iqwerty.toast.Toast('User name required!',options);
                return false;
            }else if(password==""){
                iqwerty.toast.Toast('Password  required!',options);
                return false;
            }
        $.ajax({
            url: url + "check_user",
            type: "POST",
            dataType: "json",
            data: $('form#user_check').serialize(),
             success: function (data, textStatus, jqXHR)
            {   
                //console.log(data);
              if (data.status == "success") {                  
                    iqwerty.toast.Toast('Login successfully',options);
                    setTimeout(function () {
                        window.location.href = url + 'User_dashboard';
                    }, 500)
                } else {
                  error_msg(data.error);
                    
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
}

function update_profile() {
    var name=$("#name").val();
    var email=$("#email").val();
    
    if(name==""){
        iqwerty.toast.Toast('User name required!',options);
        return false;
    }else if(email==""){
        iqwerty.toast.Toast('Email Id  required!',options);
        return false;
    }


 var formData = new FormData(document.querySelector("form#edit_profile"));
	  var x = document.getElementById("user_img");	
	for (var i = 0; i < x.files.length; i++) {	
	formData.append("user_img[]", x.files[i]);

	}
       
    $.ajax({
        url: url + "User_profile/update",
        type: "POST",
   	enctype: 'multipart/form-data',
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
	cache: false,
        success: function (data, textStatus, jqXHR)
        {
          if (data == "success") {
                      iqwerty.toast.Toast('Update profile successfully',options);
                setTimeout(function () {
                    location.reload();

                }, 500)
            } else {
                iqwerty.toast.Toast('Something Went Wrong Try Again',options);
              
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}



/* ----------------- Image Upload Preview ---------------*/
$(function () {
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            var file=this.files[0];
            var reader = new FileReader();
            var current_ele = this;
            //reader.onload = imageIsLoaded;
            reader.onload = (function (current_ele) { // here we save variable 'file' in closure
                 return function (e) { // return handler function for 'onload' event
                    $(current_ele).parents('.imgPreview').find('img').attr('src', this.result);
                 }
            })(current_ele);
            reader.readAsDataURL(this.files[0]);
        }
    });
});

function imageIsLoaded(e) {
    //alert($(current_ele).attr('accept'))
    $('.imgPreview').find('img').attr('src', e.target.result);
};
/* ----------------- Image Upload Preview Close---------------*/




/* ------------- Calender ------------------*/
/*$(document).ready(function(){
    $('.date').bootstrapMaterialDatePicker
    ({
        format: "DD-MM-YYYY",
        time: false,
        clearButton: false,
        minDate : new Date() ,
    });            
});*/
/* ------------- Calender Close ------------------*/


$('#myCarousel').carousel({
    interval: 1500
});


