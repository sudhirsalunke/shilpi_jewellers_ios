<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kolkata extends CI_Controller {

  public function __construct() {
      parent::__construct(); 
  	}

	public function index()
	{	
	  $data['display_title']="KOLKATA";	
   	$this->load->view('common/header',$data);
    $this->load->view('kolkata');
  
	}
}  
  ?>