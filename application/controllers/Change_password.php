<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_password extends CI_Controller {

  public function __construct() {
      parent::__construct();        
      $this->load->config('admin_validationrules', TRUE);
      $this->load->config('email');
      $this->load->library('email_lib');
      $this->load->library('data_encryption');
      $this->load->helper(array('common_helper'));
      $this->load->model(array('User_model'));
    }

  public function index()
  {

    $customer_id=$_SESSION['user_id'];
    $data['order_data']=$this->User_model->get_order_by_user_id($customer_id); 
    $data['display_title']='Profile';
    $this->load->view('common/header',$data);
    $this->load->view('change_password',$data);
    $this->load->view('common/footer',$data);

  }
  public function update_password_by_id(){
      $postData =$this->input->post();
       // print_r($postData);die;
      $ch_pass= $this->User_model->check_password_by_id($postData);
      // echo $this->db->last_query();
      // print_r($ch_pass);die;
      if(!empty($ch_pass)){
        $res= $this->User_model->update_password_by_id($postData);
          if($res == TRUE){
              $response_data['status']= 'success';
              $response_data['data']= null;
              $response_data['msg']= 'Password Successfully changed';
          }
      }else{
        $response_data['status']= 'failure';
        
      }

      echo json_encode($response_data);
  }
  public function forget_password(){
     $postData =$this->input->post();

        $validation_result = $this->User_model->validateForgotPassswordData();

      //print_r($validation_result);die;
        if($validation_result == FALSE)
        {  
            $response_data['status']= 'failure';
            $response_data['data']= null;
            $response_data['error'] = array('email'=>strip_tags(form_error('email')));
        }else{
            $data['encrypted_password'] = uniqid("pass");
            $res = $this->User_model->forgot_password($postData['email'],$data);
            if($res == TRUE){
                 $response_data['status']= 'success';
                 $response_data['data']= null; 
                 $response_data['msg']= 'Reset password link send to your mail'; 
            }
        }
        //print_r($response_data);die;
        echo json_encode($response_data);
 }
}
?>