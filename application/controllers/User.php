<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {

  public function __construct() {
      parent::__construct(); 

      $this->load->config('admin_validationrules', TRUE);
      $this->load->library('data_encryption');
      $this->load->helper(array('common_helper'));
      $this->load->model(array('User_model'));
  	}

	public function index()
	{	
   
    $this->load->view('user_index');
  
	}


	public function check_user()
    {   
    // print_r($_POST);die;
    	if ($this->session->userdata('logged_in') == TRUE) {
          $data['status']="success";
          echo json_encode($data);die; 
            //redirect(ADMIN_PATH . 'User_dashboard');
        } else {
            if (!empty($_POST)) { 

                    $login_validation_result = $this->User_model->validateAdminLogin();
                 // print_r($login_validation_result);die;
                    if ($login_validation_result['status'] == 'failure') {
                        
                        echo json_encode($login_validation_result);die;
                    }
                    else{
                        $getuserdetails = userdataFromUsername($this->input->post('username'));
                  

                        $session_data = array(
                            'user_id' => $getuserdetails['id'],
                            'name' => $getuserdetails['name'],
                            'middle_name' => $getuserdetails['middle_name'],
                            'last_name' => $getuserdetails['last_name'],
                            'email' => $getuserdetails['email'],
                            'company_name' => $getuserdetails['company_name'],
                            'mobile_no' => $getuserdetails['mobile_no'],
                            'client_code' => $getuserdetails['client_code'],
                            'logged_in' => TRUE
                        );
                        $this->session->set_userdata($session_data);
                        if ($login_validation_result['status'] == 'success') {     
                       	// print_r($session_data);die;      
                          echo json_encode($login_validation_result);die;           
                           
                    }
                }
            }else{
                $data['page_title'] = 'Sign In';
                $this->load->view('user_index',$data);
            }
            
        }
    }   
    public function check_user_profile() {
          $postdata=$this->input->post();   
     //print_r($postdata);die;
            if (!empty($postdata)) { 
                unset($_POST['conf_password']);
            $UserProfile_validation_result = $this->User_model->validateUserProfile();
            if ($UserProfile_validation_result['status'] == 'failure') {
                
                echo json_encode($UserProfile_validation_result);die;
            }else{
                   $data=$this->User_model->create_user_info();
                 echo json_encode($data);
            }
        }
    }



    public function logout() {
        session_destroy();
        $data['page_title'] = 'Sign In';
        $this->load->view('welcome',$data);
    } 

}
