<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View_change_history extends CI_Controller {

  public function __construct() {
      parent::__construct(); 
        if (empty($this->session->userdata('user_id'))) {
        redirect(ADMIN_PATH . 'User/logout');
      }      
      $this->load->config('admin_validationrules', TRUE);
      $this->load->library('data_encryption');
      $this->load->helper(array('common_helper'));
      $this->load->model(array('Order_model'));
  	}

	public function index($order_id="")
  {  
    $data['order_history']=$this->Order_model->get_order_history($order_id);
    $data['order_id']=$order_id;
    $data['display_title']="CHANGE HISTORY";
    $this->load->view('common/header',$data);
    $this->load->view('view_chnage_history',$data);
    $this->load->view('common/footer',$data);
	}
}
?>