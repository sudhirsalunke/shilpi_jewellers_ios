<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_edit extends CI_Controller {

  public function __construct() {
      parent::__construct(); 
      if (empty($this->session->userdata('user_id'))) {
        redirect(ADMIN_PATH . 'User/logout');
      }     
      $this->load->config('admin_validationrules', TRUE);
      $this->load->library('data_encryption');
      $this->load->helper(array('common_helper'));
      $this->load->model(array('Order_model'));
  	}

	public function index($order_id="")
	{

    $data['order_details']=$this->Order_model->get_order($order_id); 
    $data['order_img_details']=$this->Order_model->get_images_by_order_id($order_id); 
    // print_r($data['order_img_details']);       
    $data['parent_category'] = $this->Order_model->get_parent_category();
    $data['display_title']='UPDATE ORDER';
    $this->load->view('common/header',$data);
    $this->load->view('order_edit',$data);
    $this->load->view('common/footer',$data);
	}
    private function validation_result($update=""){
      $data = array();
      $allowed_ext=array('jpg','jpeg','png');

      //print_r($_POST['customer_order']);

      $validationResult = $this->Order_model->validatepostdata();
      if($validationResult===FALSE){
        $data['status']= 'failure';
        $data['data']= '';
        $data['error'] = array(
          'order_name'=>strip_tags(form_error('customer_order[order_name]')),
          'order_date'=>strip_tags(form_error('customer_order[order_date]')),
          'parent_category_id'=>strip_tags(form_error('customer_order[parent_category_id]')),
          'weight_range_id'=>strip_tags(form_error('customer_order[weight_range_id]')),
          'quantity'=>strip_tags(form_error('customer_order[quantity]')),
          'quantity'=>strip_tags(form_error('customer_order[quantity]')),
          'size'=>strip_tags(form_error('customer_order[size]')),
          'length'=>strip_tags(form_error('customer_order[length]')), 
          'remark' => strip_tags(form_error('customer_order[remark]')),
        );
      }
      return $data;
    }



      private function validate_image(){
      //print_r($_FILES['image']['type']);exit;
      $error =array();
      $allowed_ext=array('jpg','jpeg','png');
      if($_FILES['image']['name'] != ""){

          foreach ($_FILES['image']['name'] as $key => $val )
            {  
              if($_FILES['image']['name'][$key] != "")
              { 
                  if($_FILES['image']['type'][$key] == "image/png" || $_FILES['image']['type'][$key] == "image/jpg" || $_FILES['image']['type'][$key]  == "image/jpeg"){
                       //print_r('crrct');exit;            
                  }else{
                    $error['msg']="Invalid file format, Please Upload ".implode('/',$allowed_ext)." files.";
                    $error['status']="error"; 
                   }
             }
        }
     //   print_r($error);exit;
        return $error;  
    }
  }
  public function change_status(){
    $order_id=$_POST['id'];
    //print_r($order_id);die;
    $result = $this->Order_model->change_status($order_id);
    if($result == TRUE){
      $response_data['status'] = "success";
    }else{
      $response_data['status'] = "Something Went Wrong Try Again";
      $response_data['error'] = "success";
    }
    echo json_encode($response_data);
    }
}
?>