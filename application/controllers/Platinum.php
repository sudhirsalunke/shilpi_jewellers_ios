<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Platinum extends CI_Controller {

  public function __construct() {
      parent::__construct(); 
  	}

	public function index()
	{	
   		$data['display_title']="PLATINUM";	
   		$this->load->view('common/header',$data);
    	$this->load->view('platinum');
  
	}
}  
  ?>