<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_profile extends CI_Controller {

  public function __construct() {
      parent::__construct();  
      if (empty($this->session->userdata('user_id'))) {
        redirect(ADMIN_PATH . 'User/logout');
      }    
      $this->load->config('admin_validationrules', TRUE);
      $this->load->library('data_encryption');
      $this->load->helper(array('common_helper'));
      $this->load->model(array('User_model'));
    }

  public function index()
  {

    $customer_id=$_SESSION['user_id'];
    $data['user_data']=$this->User_model->get_by_user_id($customer_id); 
    // print_r($data); 
    $data['display_title']="EDIT PROFILE";   
    $this->load->view('common/header',$data);
    $this->load->view('editprofile',$data);
    $this->load->view('common/footer',$data);
  }


  public function update(){ 
    $data = $this->User_model->update();
    echo json_encode($data);
  }
}
?>