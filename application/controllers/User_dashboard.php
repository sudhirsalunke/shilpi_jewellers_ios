<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_dashboard extends CI_Controller {

  public function __construct() {
      parent::__construct();  
      if (empty($this->session->userdata('user_id'))) {
        redirect(ADMIN_PATH . 'User/logout');
      }    
      $this->load->config('admin_validationrules', TRUE);
      $this->load->library('data_encryption');
      $this->load->helper(array('common_helper'));
      $this->load->model(array('User_model'));
    }

  public function index()
  {

    $customer_id=$_SESSION['user_id'];
    $data['user_data']=$this->User_model->get_by_user_id($customer_id); 
    $data['order_data']=$this->User_model->get_order_by_user_id($customer_id); 
    // print_r($_SESSION['name']);
    $this->load->view('common/header',$data);
    $this->load->view('dashboard',$data);
    $this->load->view('common/footer',$data);
  }
}
?>