<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_order extends CI_Controller {

  public function __construct() {
      parent::__construct();  
      if (empty($this->session->userdata('user_id'))) {
        redirect(ADMIN_PATH . 'User/logout');
      }    
      $this->load->config('admin_validationrules', TRUE);
      $this->load->library('data_encryption');
      $this->load->helper(array('common_helper'));
      $this->load->model(array('Order_model'));
  	}

	public function index()
	{
  
    $data['parent_category'] = $this->Order_model->get_parent_category();
    //print_r($data);
     $data['display_title']='Create Order';
    $this->load->view('common/header',$data);
    $this->load->view('createorder',$data);
    $this->load->view('common/footer',$data);
	}
  private function validation_result($update=""){
    $data = array();
    $allowed_ext=array('jpg','jpeg','png');

    //print_r($_POST);die;

    $validationResult = $this->Order_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'order_name'=>strip_tags(form_error('customer_order[order_name]')),
        'order_date'=>strip_tags(form_error('customer_order[order_date]')),
        'parent_category_id'=>strip_tags(form_error('customer_order[parent_category_id]')),
        'weight_range_id'=>strip_tags(form_error('customer_order[weight_range_id]')),
        'quantity'=>strip_tags(form_error('customer_order[quantity]')),        
        'size'=>strip_tags(form_error('customer_order[size]')),
        'length'=>strip_tags(form_error('customer_order[length]')), 
        'remark' => strip_tags(form_error('customer_order[remark]')),
      );
    }
    return $data;
  }

  public function store(){
  //print_r($_POST);exit;
    $data=$this->validation_result('image_optional');
  if($_FILES != ""){
    $image_data = $this->validate_image();
    if(count($image_data) != 0 ){
      $data['status']  =$image_data['status'];
      $data['msg'] = $image_data['msg'];
    }
  }
  //   print_r($data);exit;
    if (count($data)==0 && count($image_data) == 0) {
      $data = $this->Order_model->store($_POST);           
    }
    echo json_encode($data);
  }

  public function update(){
     // print_r($_FILES);exit;
      if($_FILES != ""){
          $image_data = $this->validate_image();
          if(count($image_data) != 0 ){
            $data['status']  =$image_data['status'];
            $data['msg'] = $image_data['msg'];
          }
      }
            $data=$this->validation_result('image_optional');
  //   print_r($data);exit;

      if (count($data)==0 && count($image_data) == 0) {
        $result = $this->Order_model->update_order($_POST);   
          if($result == TRUE){     
            $data['status'] = "success";
            $data['error'] = '';
            $data['order_status'] = 0;
            $data['msg'] = "Updated successfully";
          }        
      }
     
      echo json_encode($data);
  }

    private function validate_image(){
       // print_r($_FILES);die;
       // exit;
      $error =array();
      $allowed_ext=array('jpg','jpeg','png');
      if(!empty($_FILES['image']['name'][0] )){

          foreach ($_FILES['image']['name'] as $key => $val )
            {  
              //print_r($val);
              if($_FILES['image']['name'][$key] != "")
              { 
                  if($_FILES['image']['type'][$key] == "image/png" || $_FILES['image']['type'][$key] == "image/jpg" || $_FILES['image']['type'][$key]  == "image/jpeg"){
                       //print_r('crrct');exit;            
                  }else{
                    $error['msg']="Invalid file format, Please Upload ".implode('/',$allowed_ext)." files.";
                    $error['status']="error"; 
                   }
             }
        }
     //   print_r($error);exit;
    }
        return $error;  
  }
}
?>