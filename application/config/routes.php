<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'User';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['change_password'] = 'Change_password/update_password_by_id';
$route['forget_password'] = 'Change_password/forget_password';
$route['check_user'] = 'User/check_user';
$route['check_user_profile'] = 'User/check_user_profile';



$route['User_dashboard'] = 'User_dashboard/index';
$route['My_order'] = 'My_order/index';
$route['create_order'] = 'Create_order/index';
$route['order_details'] = 'Order_details/index';
$route['order_details/(:any)'] = 'Order_details/index/$1';


$route['create_order/store'] = 'Create_order/store';
$route['view_change_history'] = 'View_change_history/index';
$route['view_change_history/(:any)'] = 'View_change_history/index/$1';
$route['create_order/update'] = 'Create_order/update';
$route['change_status'] = 'Order_edit/change_status';
// $route['order_edit/change_status/(:any)'] = 'Order_edit/change_status/$1';
$route['order_edit'] = 'Order_edit/index';
$route['order_edit/(:any)'] = 'Order_edit/index/$1';