<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['login'] = array(
    array(
        'field' => 'username',
        'label' => 'Email',
        'rules' => 'trim|required|verifyCredentials'
    ),
    array(
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'trim|required|md5'
    )
);
$config['UserProfile'] = array(

    array(
        'field' => 'name',
        'label' => 'name',
        'rules' => 'trim|required',
        'errors' =>array(
            'required'=>"Please Enter Name",            
            ),
    ),
    
    array(
        'field' => 'email',
        'label' => 'email',
        'rules' => 'trim|required|check_customer_email_exists',
         'errors' =>array(
            'required'=>"Please Enter Valid Email Id",            
            ),
        
    ),
    array(
        'field' => 'mobile',
        'label' => 'Mobile No',
        'rules' => 'trim|required|exact_length[10]|numeric',
        'errors' =>array(
            'exact_length'=>"Please Enter Valid Mobile Number",
            'numeric'=>"Please Enter Digits not characters",
            ),
    ),
    array(
        'field' => 'company',
        'label' => 'Company Name',
        'rules' => 'trim|required'
    ),
    
    array(
        'field' => 'password',
        'label' => 'password',
        'rules' => 'trim|required',
 
    ),
  
);


$config['customer_order'] = array(
       array(
        'field' => 'customer_order[order_name]',
        'label' => 'Order Name',
        'rules' => 'trim|required',

    ),
    array(
        'field' => 'customer_order[delievery_date]',
        'label' => 'Delivery Date',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Delivery Date"),
        
    ),
    array(
        'field' => 'customer_order[parent_category_id]',
        'label' => 'Parent Category',
        'rules' => 'trim|required',

        
    ),
    array(
        'field' => 'customer_order[weight_range_id]',
        'label' => 'Weight Range',
        'rules' => 'trim|required',      

        
    ),
    array(
        'field' => 'customer_order[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|regex_match[/^[A-Za-z0-9_ ]*$/]',
        'errors'=>array('regex_match'=>"Please Enter Valid Quantity",
                        /*'is_natural_no_zero'=>"Please Enter Valid Quantity"*/),
        
    ),

      array(
        'field' => 'customer_order[quantity_type]',
        'label' => 'Quantity type',
        'rules' => 'trim|required',
       
    ),



);

$config['customer_order_remark'] = array(    
      array(
        'field' => 'customer_order[order_name]',
        'label' => 'Order Name',
        'rules' => 'trim|required',

    ),
    array(
        'field' => 'customer_order[delievery_date]',
        'label' => 'Delivery Date',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Delivery Date"),
        
    ),
    array(
        'field' => 'customer_order[parent_category_id]',
        'label' => 'Parent Category',
        'rules' => 'trim|required',
        //'errors'=>array('check_parent_category_id_code'=>"This Parent Category Not Exits"),
        
    ),
    array(
        'field' => 'customer_order[weight_range_id]',
        'label' => 'Weight Range',
        'rules' => 'trim|required',      

        
    ),
    array(
        'field' => 'customer_order[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|regex_match[/^[A-Za-z0-9_ ]*$/]',
        'errors'=>array('regex_match'=>"Please Enter Valid Quantity",
                        /*'is_natural_no_zero'=>"Please Enter Valid Quantity"*/),
        
    ),

      array(
        'field' => 'customer_order[quantity_type]',
        'label' => 'Quantity type',
        'rules' => 'trim|required',
       
    ),


    array(
        'field' => 'customer_order[remark]',
        'label' => 'Remark',
        'rules' => 'trim|required',
         'errors'=>array('required'=>"Please Enter Remark"),
    )
);

$config['forgot_password'] =  array(
    array(
        'field'=>'email',
        'label'=>'Email',
        'rules'=>'trim|required|valid_email|checkFORGOTEMAIL',
        'errors'=>array(
                    'checkFORGOTEMAIL'=>'Invalid Email'
                  )
    ),
   
);
?>