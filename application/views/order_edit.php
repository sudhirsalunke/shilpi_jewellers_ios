<div class="container-fluid whitebg">
	<div class="frmLogin creatOrder">
	    <form name="customer_order_form" id="customer_order_form" role="form" class="form-createOrder m-t-25" method="post">
			<div class="accordion" id="accordionExample">
				<div class="card">
				    <div class="card-header" id="headingOne">
				      <h5 class="m-b-t-0">
				        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				          Upload first image
				        </button>
				      </h5>
				    </div>
				    <div id="collapseOne" class="collapse in" aria-labelledby="headingOne" data-parent="#accordionExample">
				      <div class="card-body">
			
				       <div class="input-group image-preview imgUploadView">
				       	<?php if(!empty($order_img_details[0]['img_file_path'])) { ?>

				      <img src="<?=SERVER_IMAGES.'/'.$order_img_details[0]['img_file_path']?>" alt="<?=SERVER_IMAGES.'/'.$order_img_details[0]['img_file_path']?>">
			                <!-- <input type="text" class="form-control image-preview-filename" disabled="disabled"> -->

			                <input type="hidden" name="image_ids[0]" value="<?=$order_img_details[0]['id']?>">
			 					<?php	} else {  ?>
			 				<img src="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" alt="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" class="default_order_img_upload">

	 						<?php } ?>
			                 <!-- don't give a name === doesn't send on POST/GET -->
			                <span class="input-group-btn">
			                    <!-- image-preview-clear button -->
			                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
			                         Clear
			                    </button>
			                    <!-- image-preview-input -->
			                    <div class="btn btn-default image-preview-input">		                        
			                        <span class="title">Upload your image here</span>
			                        <span class="image-preview-input-title">Browse</span>
			                       <input type="file" accept="image/png, image/jpeg, image/gif" id="image_0"/> <!-- rename it -->
			                    </div>
			                </span>
			            </div><!-- /input-group image-preview [TO HERE]--> 
				      </div>
				    </div>
				</div>
			  <div class="card">
			    <div class="card-header" id="headingTwo">
			      <h5 class="m-b-t-0">
			        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			          Upload Second image 

			        </button>
			      </h5>
			    </div>
		<?php if(!empty($order_img_details[1]['img_file_path'])) { $collapse= "collapse in"; }else { $collapse= "collapse";}?>
				
				<div id="collapseTwo" class="<?= $collapse?>" aria-labelledby="headingTwo" data-parent="#accordionExample">
			      <div class="card-body">
				       <div class="input-group second-image-preview imgUploadView">
				       <?php if(!empty($order_img_details[1]['img_file_path'])) { ?>
	       		 	<img src="<?=SERVER_IMAGES.'/'.$order_img_details[1]['img_file_path']?>" alt="<?=SERVER_IMAGES.'/'.$order_img_details[1]['img_file_path']?>" >
			                <!-- <input type="text" class="form-control second-image-preview-filename" disabled="disabled"> -->

			              <input type="hidden" name="image_ids[1]" value="<?=$order_img_details[1]['id']?>">
		 				<?php } else { ?>
				         <img src="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" alt="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" class="second_default_order_img_upload">
				        <?php } ?>	
			                 <!-- don't give a name === doesn't send on POST/GET -->
			                <span class="input-group-btn">
			                    <!-- image-preview-clear button -->
			                    <button type="button" class="btn btn-default second-image-preview-clear" style="display:none;">
			                        <!-- <span class="glyphicon glyphicon-remove"></span> --> Clear
			                    </button>
			                    <!-- image-preview-input -->
			                    <div class="btn btn-default second-image-preview-input">
			                    	<span class="title">Upload your image here</span>
			                        <span class="second-image-preview-input-title">Browse</span>
			                      <input type="file" accept="image/png, image/jpeg, image/gif" id="image_1"> <!-- rename it -->
			                    </div>
			                </span>
			            </div><!-- /input-group image-preview [TO HERE]--> 
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" id="headingThree">
			      <h5 class="m-b-t-0">
			        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			          Upload Third image 
			        </button>
			      </h5>
			    </div>
			     <?php if(!empty($order_img_details[2]['img_file_path'])) { $collapse= "collapse in"; }else { $collapse= "collapse"; } ?>
				
			    <div id="collapseThree" class="<?= $collapse;?>" aria-labelledby="headingThree" data-parent="#accordionExample">
			      <div class="card-body">
			         <!-- <div class="files">
				        <input type="file" name="" >
				       </div> -->
				       <div class="input-group third-image-preview imgUploadView">
				        <?php if(!empty($order_img_details[2]['img_file_path'])) { ?>

						 	<img src="<?=SERVER_IMAGES.'/'.$order_img_details[2]['img_file_path']?>" alt="<?=SERVER_IMAGES.'/'.$order_img_details[2]['img_file_path']?>">
			                <!-- <input type="text" class="form-control third-image-preview-filename" disabled="disabled"> --> 

			                <input type="hidden" name="image_ids[2]" value="<?=$order_img_details[2]['id']?>">
						 	<?php	} else { ?>
				             <img src="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" alt="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" class="third_default_order_img_upload">
				             	<?php } ?>
			                <!-- don't give a name === doesn't send on POST/GET -->
			                <span class="input-group-btn">
			                    <!-- image-preview-clear button -->
			                    <button type="button" class="btn btn-default third-image-preview-clear" style="display:none;">
			                        <!-- <span class="glyphicon glyphicon-remove"></span> --> Clear
			                    </button>
			                    <!-- image-preview-input -->
			                    <div class="btn btn-default third-image-preview-input">
			                        <span class="title">Upload your image here</span>
			                        <span class="third-image-preview-input-title">Browse</span>

			                        <input type="file" accept="image/png, image/jpeg, image/gif"  id="image_2" /> <!-- rename it -->


			                    </div>
			                </span>
			            </div><!-- /input-group image-preview [TO HERE]--> 
			      </div>
			    </div>
			  </div>
			</div>
			<?php
			   $type=array(
	   				array('name'=>'Inches'),
	   				array('name'=>'mm'),
	   				array('name'=>'cmm'),
	   				);
                
                 $purity=array(
	   				array('name'=>'22kt'),
	   				array('name'=>'21kt'),
	   				array('name'=>'18kt'),
	   				array('name'=>'other'),
	   				); 
	   			$quantity_type=array(
	   				array('name'=>'pieces'),
	   				array('name'=>'pair'),	   				
	   				);       	      
			?>
			<input type="hidden" id="order_id"  name="customer_order[id]"  value="<?=$order_details['id']?>">
		<input type="hidden" id="status"  name="customer_order[status]"  value="<?=$order_details['co_status']?>">
				<div class="form-group m-t-25">
			<label for="orderName">Order Name</label>
			<div class="Name">
                <input type="text" placeholder="Enter Order Name" id="order_name" class="form-control order_name" name="customer_order[order_name]"  value="<?=$order_details['order_name']?>">
                <span class="text-danger" id="order_name_error"></span>
            </div>
		</div>
			<div class="form-group ">
		   <div class="row">
		   	  <div class="col-xs-6">
		   	  	<label for="orderName">Delivery Date</label>
				<div class="Name">
	          		<div class="form-control-wrapper">
						<input type="text" name="customer_order[delievery_date]" id="deliverydate" class="form-control floating-label dateInput" placeholder="Enter Delivery Date" value="<?=@$order_details['delievery']?>"  data-date-start-date="0d">
					</div>	                  
	                <span class="text-danger" id="order_date_error"></span>
	            </div>
		   	  </div>
		   	  <div class="col-xs-6">
		   	  	<label for="orderName">Item</label>
				<div class="Name">
	                <select class="form-control" name="customer_order[parent_category_id]" id="parent_category_id">
	                <?php foreach ($parent_category as $key =>$value){ ?>
	                	<option value="<?=$value['name'];?>" <?= ($value['name']==$order_details['parent_category_id']) ? 'selected' : ''?>><?=$value['name'];?></option>
	     			<?php } ?>
	                </select>
	                 <span class="text-danger" id="parent_category_id_error"></span>
	            </div>
		   	  </div>
		   </div>
		</div>
		<div class="form-group ">
		   <div class="row">
		   	  <div class="col-xs-6">
		   	  	<label for="orderName">Length</label>
				<div class="Name">
	                <input class="form-control" type="number" placeholder="Enter Length" name="customer_order[length]" id="length" value="<?=$order_details['length']?>">
	                 <span class="text-danger" id="length_error"></span>
	            </div>
		   	  </div>
		   	  <div class="col-xs-6">
		   	  	<label for="orderName">Length Type</label>
				<div class="Name">

	               <select class="form-control" name="customer_order[length_type]" id="length_type">
	         	       <?php foreach ($type as $key =>$value){ ?>
	                	<option value="<?=$value['name'];?>" <?= ($value['name']==$order_details['length_type']) ? 'selected' : ''?>><?=$value['name'];?></option>
	     			<?php } ?>                        
	                </select>
	                 <span class="text-danger" id="length_type_error"></span>
	            </div>
		   	  </div>
		   </div>
		</div>
		<div class="form-group ">
		   <div class="row">
		   	  <div class="col-xs-6">
		   	  	<label for="orderName">Width</label>
				<div class="Name">
	                <input class="form-control" type="number" placeholder="Enter Width"  name="customer_order[width]" id="width" value="<?=$order_details['width']?>">
	                 <span class="text-danger" id="width_error"></span>
	            </div>
		   	  </div>
		   	  <div class="col-xs-6">
		   	  	<label for="orderName">Width Type</label>
				<div class="Name">	            
	                <select class="form-control" name="customer_order[width_type]" id="width_type">
	                 <?php foreach ($type as $key =>$value){ ?>
	                	<option value="<?=$value['name'];?>"<?= ($value['name']==$order_details['width_type']) ? 'selected' : ''?>><?=$value['name'];?></option>
	     			<?php } ?>  
	                </select>
	                 <span class="text-danger" id="width_type_error"></span>
	            </div>
		   	  </div>
		   </div>
		</div>
		<div class="form-group ">
		   	<div class="row">
			   	<div class="col-xs-6">
			   	  	<label for="orderName">Quantity</label>
					<div class="Name">
		                <input class="form-control" type="number" placeholder="Enter Quantity" name="customer_order[quantity]" id="quantity" value="<?=$order_details['quantity']?>">
		                 <span class="text-danger" id="quantity_error"></span>
		            </div>
			   	</div>
		   	  	<div class="col-xs-6">
		   	  		<label for="orderName">Quantity Type</label>
			
	                <select class="form-control" id="quantity_type" name="customer_order[quantity_type]">
	                   <?php foreach ($quantity_type as $key =>$value){ ?>
	                	<option value="<?=$value['name'];?>" <?= ($value['name']==$order_details['quantity_type']) ? 'selected' : ''?>><?=$value['name'];?></option>
	     			<?php } ?>   
	                </select>
	                 <span class="text-danger" id="quantity_type_error"></span>
	            </div>
		   	</div>
		</div>	
			
		<div class="form-group ">
		   <div class="row">
		   	  <div class="col-xs-6">
		   	  	<label for="orderName">Weight</label>
				<div class="Name">
	                <input class="form-control" type="number" placeholder="Enter Weight"   name="customer_order[weight_range_id]" id="weight_range_id" value="<?=$order_details['weight_range_id']?>">
	                 <span class="text-danger" id="weight_range_id_error"></span>
	            </div>
		   	  </div>
		   	  <div class="col-xs-6">
		   	  	<label for="orderName">Purity</label>
				<div class="Name">	           
	               <select class="form-control"  name="customer_order[purity]" id="purity">
	              <?php foreach ($purity as $key =>$value){ ?>
	                	<option value="<?=$value['name'];?>" <?= ($value['name']==$order_details['purity']) ? 'selected' : ''?>><?=$value['name'];?></option>
	     			<?php } ?>    
	                </select>
	                 <span class="text-danger" id="purity_error"></span>
	            </div>
		   	  </div>
		   </div>
		</div>
		<div class="form-group">
			<label for="orderName">Size</label>
				<div class="Name">
	                <input class="form-control" type="number" placeholder="Enter Size"  name="customer_order[size]" id="size" value="<?=$order_details['size']?>">
	                 <span class="text-danger" id="size_error"></span>
	            </div>
		</div>
		<div class="form-group">
			<label for="orderName">Remark</label>
			<div class="Name">
                <input class="form-control" type="text" placeholder="Enter Remark" name="customer_order[remark]" id="remark" value="<?=$order_details['remark']?>">
                 <span class="text-danger" id="remark_error"></span>
            </div>
		</div>
			<div class="row btnFix">
				<div class="btnSubmit ">
				 	<button type="button" class="btn btn-primary helveticabold text-uppper" id="creatOrder_btn" onclick="customer_order('update');" >Update</button>
				</div>
			</div>
			
		</form>
	</div><!--frmLogin end-->
</div><!--container-fluid end--> 	
</body>
</html>	