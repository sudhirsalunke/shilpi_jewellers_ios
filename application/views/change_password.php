	<div class="container userForm">
 		<div class="frmCreateOrder">
 			<form class="form-login m-t-25" id="update_password" name="update_password" method="post">
 			<input type="hidden" name="user_id" id="user_id" value="<?= $_SESSION['user_id'];?>">
 				<h3>Change Password</h3>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Current Password</label>
						<div class="input_pass">
	                        <input class="form-control" type="password" placeholder="Enter Current password" name="old_password" id="old_password">
	                        
	                    </div>

				</div>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">New Password</label>
						<div class="input_pass">
	                        <input class="form-control" type="password" placeholder="Enter new password" name="new_password" id="new_password">
	                        
	                    </div>
	                
				</div>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Confirm Password</label>
						<div class="input_pass">
	                        <input class="form-control" type="password" placeholder="Enter confirm password" name="confirm_password" id="confirm_password">
	                      
	                    </div>
	                
				</div>
				
				 <div class="row text-center btnSubmit m-t-50">
				 	<button type="button" class="btn btn-primary helveticabold text-uppper" onclick="update_password_data();">Update</button>
				 </div>
 			</form>
 		</div><!--frmLogin end-->
		

 		
 	</div><!--container-fluid end-->


</body>
</html>