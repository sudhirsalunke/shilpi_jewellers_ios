<form class="form-login"  role="form" id="edit_profile"  name="edit_profile" action="post" enctype="multipart/form-data">			
 	<div class="repoView">
 		<div class="frmCreateOrder">
	 		<div class="row">	 
 				<div class="bannerImg">
 					<img src="<?=ADMIN_IMAGES_PATH?>profile-page.png" alt="bombay">	 			
	 				<div class="userProfileimg slim"> 						 						
 							<?php 
 								if(!empty($user_data['imagepath'])){ ?>
								<img src="<?=ADMIN_PATH.'/'.$user_data['imagepath']?>" alt="<?=$user_data['imagepath']?>" class="profile_pic"> <?php }else{?>
		 						<img src="<?=ADMIN_IMAGES_PATH?>profile.png" />

		 					<?php } ?> 
		 					<span class="fa fa-camera upload">
 							<input type="file" id="user_img" accept="image/png, image/jpeg, image/gif">
 						</span>
                       </div>
 				</div>

	 		</div>
	 		
 		

 				<input type="hidden" name="user_id" value="<?=$_SESSION['user_id'];?>">
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Name</label>
						<div class="input_pass">
	                        <input class="form-control" type="text" placeholder="Enter your password" name="name" id="name" value="<?=$user_data['name'];?>">
	                        <span class="message" id="msg_user_password"></span>
	                    </div>

				</div>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Email ID</label>
						<div class="input_pass">
	                        <input class="form-control" type="email" placeholder="Enter your new password" name="email" id="email" value="<?=$user_data['email'];?>">
	                        <span class="message" id="msg_user_new_password"></span>
	                    </div>
	                
				</div>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Mobile No</label>
						<div class="input_pass">
	                        <input class="form-control" type="number" placeholder="mobile no" name="mobile_no"  value="<?=$user_data['mobile_no'];?>" disabled="disabled">
	                        <span class="message" id="msg_user_confirm_password"></span>
	                    </div>
	                
				</div>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Company Name</label>
						<div class="input_pass">
	                        <input class="form-control" type="text" name="company_name" value="<?=$user_data['company_name'];?>" disabled>
	                        <span class="message" id="msg_user_new_password"></span>
	                    </div>
	                
				</div>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Client code</label>
						<div class="input_pass">
	                        <input class="form-control" type="number" name="client_code" id="client_code" value="<?=$user_data['client_code'];?>" disabled>
	                        <span class="message" id="msg_user_new_password"></span>
	                    </div>
	                
				</div>
				
				 <div class="btnSubmit btnFix">
				 	<button type="button" class="btn btn-primary helveticabold text-uppper" onclick="update_profile(); ">Update</button>
				 </div>

			
 			</form>
 		</div><!--frmLogin end-->

 		
 	</div><!--container-fluid end-->