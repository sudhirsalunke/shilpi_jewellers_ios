<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Welcome</title>
</head>
<style type="text/css">
	body{
		margin:0;
		font-family: sans-serif, arial;
	}
	.welcome{
		width: 100%;
		height: 100vh;
		background-color: #fff;
		margin:0;
		position:relative;
	}
	.contMidle{
		position: absolute;
		top: 50%;
		left:50%;
		transform:translate(-50%, -50%);
	}
	.logo{
		display:block;
		width: 200px;
		height: auto;
		margin:0 auto;
	}
	.started{
		clear: both;
	    display: block;
	    background-color: #2d3091;
	    color: #ffffff;
	    border-color: #2d3091;
	    font-size: 17px;
	    white-space: nowrap;
	    padding: 8px 16px;
	    margin-top: 50px;
	    text-align: center;
	    text-decoration: none;
	}
	.contMidle h2{
		text-align: center;
		color: #d1aa65;
		text-transform: uppercase;
	}
	.started:hover{
		box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
	}
</style>
<body>
	<div class="welcome">
		<div class="contMidle">
			<h2>Welcome</h2>
			<img src="<?=ADMIN_IMAGES_PATH?>logo.png"  alt="logo" class="logo">
			<a href="<?=ADMIN_PATH?>All_department" class="started">Next</a>			
			<a href="<?=ADMIN_PATH?>All_department" class="started">Skip</a>			
		</div>
	</div>
</body>
</html>