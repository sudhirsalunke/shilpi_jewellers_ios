<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	
	<link rel="stylesheet" type="text/css" href="<?=ADMIN_CSS_PATH?>bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=ADMIN_CSS_PATH?>style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?=ADMIN_JS_PATH?>bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=ADMIN_JS_PATH?>toast.js"></script>
</head>
<body class="userForm whitebg">
 		<div class="loginbanner">
			<div class="logo">
				<img src="<?=ADMIN_IMAGES_PATH?>logo.png"  alt="logo" class="w-auto">
			</div>
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
			    <div class="carousel-inner">
			      <div class="item active opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-ANTIQUE-JEWELLERY.png" alt="THE ANTIQUE JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE ANTIQUE JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-BOMBAY-JEWELLERY.png" alt="THE BOMBAY JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE BOMBAY JEWELLERY</h3>			          
			        </div>
			      </div>			      
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-CHAINS-JEWELLERY.png" alt="THE CHAINS JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE CHAINS JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-EMERALD-JEWELLERY.png" alt="THE EMERALD JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE EMERALD JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-KOLKATA-JEWELLERY.png" alt="THE KOLKATA JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE KOLKATA JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-MANGALSUTRA-JEWELLERY.png" alt="THE MANGALSUTRA JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE MANGALSUTRA JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-PLATINUM-JEWELLERY.png" alt="THE PLATINUM JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE PLATINUM JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-TRIOS-BANGLES.png" alt="THE TRIOS BANGLES" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE TRIOS BANGLES</h3>			          
			        </div>
			      </div>			      
			    </div>
			</div><!--myCarousel end-->
		</div>
 	<div class="container">
 			
 		<div class="frmLogin">	
	 		<div class="row">
	 			<div class="text-center text-uppper">
	 				<h3 class="helveticabold">Login</h3>
	 			</div>
	 		</div>
 		
 			<form class="form-login" method="post" name="user_check" id="user_check">
				<div class="form-group">
					<label for="email">Email ID</label>
					<div class="input_email">
                 <input class="form-control" type="email" name="username" id="username"  placeholder="enter email id">                                
                </div>
				</div>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Password</label>
						<div class="input_pass">
	                       <input class="form-control" autocomplete="false" type="password" placeholder="enter password" name="password" id="password">
                                
	                    </div>
				</div>
				<div class="row forgotPass">
				   <div class="col-md-12"><a href="#" class="text-right colorBlue linkforgotPass">Forgot Password?</a></div>
				 </div>
				<div class="row text-center  m-t-25">
				   <div class="col-md-12"><p>New User? <a href="#" class="text-uppper colorBlue linkRegister">Register</a></div>
				 </div>
				 <div class="row text-center btnSubmit">
				 <!-- 	<button type="submit" class="btn btn-primary helveticabold text-uppper" name="submit" id="submit" value="SIGN IN">LOG  IN</button> -->
				 		<button type="button" class="btn btn-primary helveticabold text-uppper" onclick="check_user_profile(); " >LOG  IN</button>
				 	  
				 </div>
 			</form>
 		</div><!--frmLogin end-->

		<div class="frmRg">
			<div class="row">
	 			<div class="col-md-12 text-center text-uppper">
	 				<h3 class="helveticabold">Registration</h3>
	 			</div>
	 		</div>
		
 			<form class="form-registration m-t-25" method="post" name="create_user_profile" id="create_user_profile">
 				<div class="form-group">
					<label for="email">Name</label>
						<div class="Name">
                            <input class="form-control" type="text" placeholder="enter name" value="" name="name" id="name">
                             <span class="message" id="msg_name"></span>
                        </div>
				</div>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Email Id</label>
						<div class="input_pass">
	                        <input class="form-control" type="email" placeholder="enter email id" value="" name="email" id="user_email">
                            <span class="message" id="msg_email"></span>
	                    </div>
				</div>
				<div class="form-group m-t-25">
					<label for="pwd">Mobile No</label>
						<div class="input_pass">
	                        <input class="form-control" type="number" placeholder="enter mobile number" value="" name="mobile" id="user_mobile" pattern="\d{3}[\-]\d{3}[\-]\d{4}" maxlength="10">
                            <span class="message" id="msg_mobile"></span>
	                    </div>
				</div>
				<div class="form-group m-t-25">
					<label for="company">Company</label>
						<div class="input_pass">
	                        <input class="form-control" type="text" placeholder="enter company name" value="" name="company" id="user_company">
                            <span class="message" id="msg_company"></span>
	                    </div>
				</div>
				<div class="form-group loginPass m-t-25">
				<label for="pwd">Password</label>
					<div class="input_pass">
                        <input class="form-control" type="password" placeholder="enter password" name="password" id="user_password">
                        <span class="message" id="msg_passd"></span>
                    </div>
			    </div>
				<div class="form-group loginPass m-t-25">
					<label for="pwd">Confirm Password</label>
						<div class="input_pass">
	                        <input class="form-control" type="password" placeholder="enter confirm password" name="conf_password" id="user_conf_password">
	                        <span class="message" id="msg_passd"></span>
	                    </div>
				</div>
				
				
				 <div class="row text-center btnSubmit m-t-50">
		
				 	<button type="button" class="btn btn-primary helveticabold text-uppper" onclick="create_user(); " >Register</button>
				 </div>
 			</form>
				 <div class="row text-center  m-t-25">
				   <div class="col-md-12"><p>Already a Customer? <a href="#" class="text-uppper colorBlue linkLogin">Login</a>
				   </div>
				 </div>
 		</div><!--frmRg end-->
 		<div class="frmforgotPass">	
	 		<div class="row">
	 			<div class="col-md-12 text-center text-uppper">
	 				<h3 class="helveticabold">Forgot Password</h3>
	 			</div>
	 		</div>
 		
 			<form class="form-forgotPass m-t-25" id="forget_password" name="forget_password" method="post">
 				<div class="form-group">
					<label for="email">Email ID</label>
						<div class="input_email">
                            <input class="form-control" type="email" placeholder="enter email id" name="email" id="email">
                             <span class="message" id="msg_email"></span>
                        </div>
				</div>
				<div class="row forgotPass">
				   <div class="col-md-12"><a href="#" class="text-right colorBlue linkLogin">Login</a></div>
				 </div>
				 <div class="row text-center btnSubmit">
				 	<button type="button" class="btn btn-primary helveticabold text-uppper" onclick="forget_password_data();" >Update</button>
				 </div>
 			</form>
 		</div><!--frmLogin end-->
 		
 	</div><!--container-fluid end-->

 	<script type="text/javascript"> 	
 		
 		$(document).ready(function(){
 			$(".linkRegister").click(function(){
 				$( ".frmRg" ).show();
 				$(".frmLogin").hide();

 			});
 			$(".linkLogin").click(function(){
 				$( ".frmLogin" ).show();
 				$(".frmRg").hide();
 				$( ".frmforgotPass" ).hide();

 			});
 			$(".linkforgotPass").click(function(){
 				$( ".frmforgotPass" ).show();
 				$(".frmLogin").hide();

 			});

 		});
</script>
 	<script type="text/javascript"> 
 var options = {
	settings: {
		duration: 2000
	}
};
 var url ='<?=ADMIN_PATH?>';
 	function error_msg(data)
		{
		 
		    var i = 0;
		    {       // console.log(data);
		    for (var key in data)
		        if (i == 0)
		        {
		        	if(data[key].length){
		        		iqwerty.toast.Toast(data[key],options);
		        	}
		           
		        }
		        i++;
		    }
		}

function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };

    function forget_password_data() {		
			var email=$("#email").val();			
			if(email==""){
				iqwerty.toast.Toast('Please Enter Email Id',options);
				return false;
			}else if(!ValidateEmail(email)){
                iqwerty.toast.Toast('Please Enter Valid Email Id',options);
                return false;
            }

	    $.ajax({
	        url: url + "forget_password",
	        type: "POST",
	        dataType: "json",
	        data: $('form#forget_password').serialize(),
	         success: function (data, textStatus, jqXHR)
	        { 	
	        	console.log(data);
	          if (data.status == "success") {
	              
	                iqwerty.toast.Toast('Reset password link send to your mail',options);
	                setTimeout(function () {
	                    window.location.href = url + 'User_dashboard';
	                }, 500)
	            } else {
	          	  error_msg(data.error);
	                
	            }
	        },
	        error: function (jqXHR, textStatus, errorThrown) {
	        }
	    });
}
 

 function create_user() {
	
			var name=$("#name").val();
			var user_email=$("#user_email").val();
			var user_mobile=$("#user_mobile").val();
			var user_company=$("#user_company").val();
			var user_password=$("#user_password").val();
			var user_company=$("#user_company").val();
			var user_conf_password=$("#user_conf_password").val();
			
			if(name==""){
				iqwerty.toast.Toast('Please Enter Name',options);
				return false;
			}else if(user_email==""){
				iqwerty.toast.Toast('Please Enter Valid Email Id',options);
				return false;
			}else if(user_email==""){
				iqwerty.toast.Toast('Please Enter Valid Email Id',options);
				return false;
			}else if(!ValidateEmail(user_email)){
				iqwerty.toast.Toast('Please Enter Valid Mobile No',options);
				return false;
			}else if(user_mobile.length!=10){
				iqwerty.toast.Toast('Please Enter Valid Mobile No',options);
				return false;
			}else if(user_company==""){
				iqwerty.toast.Toast('Please Enter Company',options);
				return false;
			}else if(user_password==""){
				iqwerty.toast.Toast('Please Enter Password',options);
				return false;
			}else if(user_conf_password==""){
				iqwerty.toast.Toast('Please Enter Confirm Password',options);
				return false;
			}else if(user_conf_password!=user_password){
				iqwerty.toast.Toast('Confirm Password should be equal to password',options);
				return false;
			}
	        	
	    $.ajax({
	        url: url + "check_user_profile",
	        type: "POST",
	        dataType: "json",
	        data: $('form#create_user_profile').serialize(),
	         success: function (data, textStatus, jqXHR)
	        { 	//console.log(data.error);
	          if (data.status == "success") {
	               iqwerty.toast.Toast('Successfully Registration',options);
	               //alert(user_email);
	               $(".frmLogin").show()
	               $( ".frmRg" ).hide();
	               $("#username").val(user_email);
	               $("#password").val(user_password);
	                // setTimeout(function () {
	                //     window.location.href = url+'User';
	                // }, 500)
	            } else {
	          	  error_msg(data.error);
	                
	            }
	        },
	        error: function (jqXHR, textStatus, errorThrown) {
	        }
	    });
}

function check_user_profile() {
		
			var username=$("#username").val();
			var password=$("#password").val();
			
			if(username==""){
				iqwerty.toast.Toast('Email id required!',options);
				return false;
			}else if(password==""){
				iqwerty.toast.Toast('Password  required!',options);
				return false;
			}
	    $.ajax({
	        url: url + "check_user",
	        type: "POST",
	        dataType: "json",
	        data: $('form#user_check').serialize(),
	         success: function (data, textStatus, jqXHR)
	        { 	
	        	console.log(data);
	          if (data.status == "success") {
	              
	                iqwerty.toast.Toast('Login successfully',options);
	                setTimeout(function () {
	                    window.location.href = url + 'User_dashboard';
	                }, 500)
	            } else {
	          	  error_msg(data.error);
	                
	            }
	        },
	        error: function (jqXHR, textStatus, errorThrown) {
	        }
	    });
}
$('#myCarousel').carousel({
    interval: 1500
});

 	</script>

</body>
</html>