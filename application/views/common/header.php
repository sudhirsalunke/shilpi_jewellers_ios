<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
	<title>SHILPI JEWELS</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.css" />
	<link rel="stylesheet" type="text/css" href="<?=ADMIN_CSS_PATH?>bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="<?=ADMIN_CSS_PATH?>style.css">	
	<link rel="stylesheet" type="text/css" href="<?=ADMIN_CSS_PATH?>slim.min.css">	

	
</head>
<?php 	$url=$this->uri->segment(1);
	if($url =="User_profile" || $url =="create_order"|| $url =="Change_password"){
		$class="whitebg";
	}else{
		$class="";
	}
?>
<body class="<?= $class;?>">
<div class="menuBar">
				<!-- <img src="<?=ADMIN_IMAGES_PATH?>Back-Arrow-2.jpg" alt="Back-Arrow-2" class=""> -->				
	<?php if($url =="User_dashboard"){ ?>
	<div class="dasbordBar">
		<div class="frmDashboard">	
			<div class="btnBox">
				<button type="button" class="slide-toggle menuButton">
					<span class="fa fa-bars" aria-hidden="true"></span>
				</button>
			</div>

			<div class="tittleBox">
				<h3 class="tittle helveticabold">SHILPI JEWELS</h3>
			</div>			
			<div class="shilpiLogo">
				<img src="<?=ADMIN_IMAGES_PATH?>Logo-1.png" alt="Logo-1">
			</div>
		</div>
		<div class="orderDet"><b>My Order</b> : <?= count($order_data)?> Orders Placed</div>			
	</div>
	<?php } 
	if($url !="User_dashboard"){ ?>
	<div class="backbtnBar">
		<div class="frmDashboard">	
			<div class="btnBox">
				<a href="<?=ADMIN_PATH?>User_dashboard"><img src="<?=ADMIN_IMAGES_PATH?>Back-Arrow-2.jpg" alt="Back-Arrow-2"></a>
			</div>

			<div class="tittleBox">
				<h3 class="tittle helveticabold fontSize20"><?=@$display_title;?></h3>
			</div>			
			<div class="shilpiLogo">
				<img src="<?=ADMIN_IMAGES_PATH?>Logo-1.png" alt="Logo-1">
			</div>
		</div>	
	</div>
	<?php } ?>
</div>
<?php 	
	if($url =="User_dashboard"){
?>
<div class="box sliderMenu">
	<div class="profileInfo">
		<div class="user">
			<?php if(!empty($user_data['imagepath'])){ ?>
			<img src="<?=ADMIN_PATH.'/'.$user_data['imagepath']?>" alt="<?=$user_data['imagepath']?>" class="profile_pic"> <?php }else{?>
			<div class="profile"><img src="<?=ADMIN_IMAGES_PATH?>profile.png"/></div>
				
			<?php } ?>	 
			<div class="details">
				<name><?=$user_data['name'];?></name>
				<a class="email"><?=$user_data['email']?></a>
			</div>	
			
		</div>
	</div>
	<!-- <button type="button" class="slide-toggle m-t-25">
		<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
	</button> -->
	<ul>	 				
		<li class="active">
			<a href="<?=ADMIN_PATH?>User_profile">
				<i class="glyphicon glyphicon-user" aria-hidden="true"></i> <span>Profile</span>
			</a>
		</li>
		<li>
			<a href="<?=ADMIN_PATH?>create_order">
				<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> <span>Create Order</span>
			</a>
		</li>
		<li>
			<a href="<?=ADMIN_PATH?>My_order">
				<i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i> <span>My Order</span>
			</a>
		</li>
		<li>
			<a href="<?=ADMIN_PATH?>Change_password">
				<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> <span>Change Password</span> 
			</a>
		</li>
		<li>
			<a href="<?=ADMIN_PATH?>User/logout">
				<i class="glyphicon glyphicon-log-in" aria-hidden="true"></i> <span>Logout</span>
			</a>
		</li>	 				
	</ul>
</div>
<?php } ?>