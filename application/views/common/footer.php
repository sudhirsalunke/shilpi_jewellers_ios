<div id="slideMenubg" style="display: none;"></div>

<div class="alertBox" style="display: none;">
    <div class="alert">
        <p>Do you want to cancel this order ?</p>
        <div class="pull-right" id="alert_Action">
            <span class="btntext" id="No">No</span>
            <span class="btntext" id="Yes">Yes</span>
        </div>
    </div>    
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_JS_PATH?>bootstrap.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_JS_PATH?>bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_JS_PATH?>hammer.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_JS_PATH?>toast.js"></script>
<script type="text/javascript" src="<?=ADMIN_JS_PATH?>slim.kickstart.min.js"></script>

<!-- Calender 
<script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_JS_PATH?>/bootstrap-material-datetimepicker.js"></script>
 Calender -->


 <script>
 var url="<?= ADMIN_PATH ?>";
 </script>
<script type="text/javascript" src="<?=ADMIN_JS_PATH?>common.js"></script>
<?php 	$url=$this->uri->segment(1);
		if($url =="User_dashboard"){ 
?>
<script type="text/javascript">
/* ------------------ Slide Menu -----------------*/
$(document).ready(function(){
    $(".menuButton").on("click", function(){
        $(".sliderMenu").addClass("menuOpen");
        $("#slideMenubg").fadeIn("slow");
        $(".sliderMenu").removeClass("menuClose");
        $("body").addClass("fixed");
    });
    $("#slideMenubg").on("click", function(){
        $(".sliderMenu").removeClass("menuOpen");
        $(".sliderMenu").addClass("menuClose");
        $(this).fadeOut("slow");
        $("body").removeClass("fixed");
    });
});

/* ----------------- Swipe Menu -----------------*/
window.onload=function(){
    var swipeRight = document.getElementById('swipe');
    var swipeLeft = document.getElementById('slideMenubg');

    // create a simple instance
    // by default, it only adds horizontal recognizers
    var swRt = new Hammer(swipeRight);
    var swlt = new Hammer(swipeLeft);

    // listen to events...
    swRt.on("swiperight", function(ev) {
        $(".sliderMenu").addClass("menuOpen");
        $("#slideMenubg").fadeIn("slow");
        $(".sliderMenu").removeClass("menuClose");
        $("body").addClass("fixed");
    });

    swlt.on("swipeleft", function(ev) {
        $(".sliderMenu").removeClass("menuOpen");
        $(".sliderMenu").addClass("menuClose"); 
        $("#slideMenubg").fadeOut("slow");
        $("body").removeClass("fixed");
    });
}
/* ----------------- Swipe Menu -----------------*/
</script>
<?php } ?>