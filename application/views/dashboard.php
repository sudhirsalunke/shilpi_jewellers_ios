 	<div class="proContainer container" id="swipe">
	 	<div class="dataBox">
	 		<?php
	 		if(count($order_data) >= 1){	 	
	 		 foreach($order_data as $key =>$value){  ?>
			 	<div class="mtb15">
	 				<a href="<?=ADMIN_PATH?>order_details/<?=$value['id']?>">
			 		    <div class="whitebg p-all5 clearfix borderradius8">
			 				<div class="col-md-8 col-sm-2  col-xs-4 proimgBox">
			 					<div class="proImg">
				 					<?php if(!empty($value['images'][0]['img_file_path'])) { ?>
				 					<img src="<?=SERVER_IMAGES.'/'.$value['images'][0]['img_file_path']?>" alt="<?=@$value['images'][0]['img_file_path']?>">
				 					<?php	} else { ?>
				 				
				 					<img src="<?=ADMIN_IMAGES_PATH.'/default_order_image.jpg'?>" alt="<?=ADMIN_IMAGES_PATH.'/default_order_image.jpg'?>">

				 					<?php } ?>	 						
			 					</div>
			 				</div>
			 				<div class="col-md-8 col-sm-10 col-xs-8 dashboardData">
			 					<div class="row">
			 						<div class="itemTitle"><?=$value['status'];?></div>
			 						<div class="col-md-6 col-xs-6 p-l-0 helveticalight">Order No :</div>
			 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$value['id']?></div>
			 						<div class="col-md-6 col-xs-6 p-l-0 helveticalight">Order Date :</div>
			 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$value['order_date']?></div>
			 						<div class="col-md-6 col-xs-6 p-l-0 helveticalight">Delivery Date :</div>
			 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$value['delievery_date']?></div>
			 						<div class="col-md-6 col-xs-6 p-l-0 helveticalight">Qty :</div>
			 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$value['quantity']?></div>
			 						<div class="col-md-6 col-xs-6 p-l-0 helveticalight">Weight :</div>
			 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$value['weight_range_id']?></div>
			 					</div>
			 				</div>
			 			</div>
		 			</a>
			 	</div>
	 		<?php	} 	
	 		}else{ ?>
	 			
	 		<div class="noorder">
	 			<img src="<?=ADMIN_IMAGES_PATH?>blanck_order.png">
	 			<h2>No Active Order</h2>
	 			<p>There are no recent orders to show</p>
	 		</div>
	 		<?php } ?>
	 	</div>
	 	<div class="row btnFix">
			<div class="btnSubmit ">
			  	<button type="button" class="btn btn-md btn-primary helveticabold text-uppper" onclick="location.href = '<?=ADMIN_PATH?>create_order';">Create new order</button>
			</div>	 		
	 	</div>
 	</div><!--frmLogin end-->
 	<?php if(count($order_data) <= 1){	 ?>	 
 	<div class="loginbanner bannerBottom">			
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
		    <div class="carousel-inner">
			      <div class="item active opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-ANTIQUE-JEWELLERY.png" alt="THE ANTIQUE JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE ANTIQUE JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-BOMBAY-JEWELLERY.png" alt="THE BOMBAY JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE BOMBAY JEWELLERY</h3>			          
			        </div>
			      </div>			      
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-CHAINS-JEWELLERY.png" alt="THE CHAINS JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE CHAINS JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-EMERALD-JEWELLERY.png" alt="THE EMERALD JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE EMERALD JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-KOLKATA-JEWELLERY.png" alt="THE KOLKATA JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE KOLKATA JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-MANGALSUTRA-JEWELLERY.png" alt="THE MANGALSUTRA JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE MANGALSUTRA JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-PLATINUM-JEWELLERY.png" alt="THE PLATINUM JEWELLERY" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE PLATINUM JEWELLERY</h3>			          
			        </div>
			      </div>
			      <div class="item opacity">
			        <img src="<?=ADMIN_IMAGES_PATH?>THE-TRIOS-BANGLES.png" alt="THE TRIOS BANGLES" style="width:100%;">
			        <div class="carousel-caption">
			          <h3>THE TRIOS BANGLES</h3>			          
			        </div>
			      </div>			      
			    </div>
		</div><!--myCarousel end-->
	</div>
	<?php } ?>	
 	</div><!--container-fluid end-->

	
</body>
</html>