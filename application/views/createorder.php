<div class="container-fluid whitebg">
	<div class="frmLogin creatOrder">
	    <form name="customer_order_form" id="customer_order_form" role="form" class="form-createOrder m-t-25" method="post" data-remote=true">
			<div class="accordion" id="accordionExample">
				<div class="card">
				    <div class="card-header" id="headingOne">
				      <h5 class="m-b-t-0">
				        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				          Upload first image
				        </button>
				      </h5>
				    </div>
				    <div id="collapseOne" class="collapse in" aria-labelledby="headingOne" data-parent="#accordionExample">
				      <div class="card-body">
				      <!--  <div class="files">
				        <input type="file" name="" >
				       </div> -->
				       
				    <div class="input-group image-preview imgUploadView">
				       <img src="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" alt="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" class="default_order_img_upload">
			                <!-- <input type="text" class="form-control image-preview-filename" disabled="disabled"> --> <!-- don't give a name === doesn't send on POST/GET -->
			                <span class="input-group-btn">
			                    <!-- image-preview-clear button -->
			                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
			                         Clear
			                    </button>
			                    <!-- image-preview-input -->
			                    <div class="btn btn-default image-preview-input">		                        
			                        <span class="title">Upload your image here</span>
			                        <span class="image-preview-input-title">Browse</span>
			                        <input type="file" accept="image/png, image/jpeg, image/gif"  id="image_0" /> <!-- rename it -->
			                    </div>
			                </span>
			            </div><!-- /input-group image-preview [TO HERE]--> 
				    </div>
				    </div>
				</div>
			  <div class="card">
			    <div class="card-header" id="headingTwo">
			      <h5 class="m-b-t-0">
			        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			          Upload Second image 

			        </button>
			      </h5>
			    </div>
			    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
			      <div class="card-body">
				       <div class="input-group second-image-preview imgUploadView">
				         <img src="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" alt="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" class="second_default_order_img_upload">
			                <!-- <input type="text" class="form-control second-image-preview-filename" disabled="disabled"> --> <!-- don't give a name === doesn't send on POST/GET -->
			                <span class="input-group-btn">
			                    <!-- image-preview-clear button -->
			                    <button type="button" class="btn btn-default second-image-preview-clear" style="display:none;">
			                        <!-- <span class="glyphicon glyphicon-remove"></span> --> Clear
			                    </button>
			                    <!-- image-preview-input -->
			                    <div class="btn btn-default second-image-preview-input">
			                    	<span class="title">Upload your image here</span>
			                        <span class="second-image-preview-input-title">Browse</span>
			                        <input type="file" accept="image/png, image/jpeg, image/gif"  id="image_1" /> <!-- rename it -->
			                    </div>
			                </span>
			            </div><!-- /input-group image-preview [TO HERE]--> 
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" id="headingThree">
			      <h5 class="m-b-t-0">
			        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			          Upload Third image 
			        </button>
			      </h5>
			    </div>
			    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
			      <div class="card-body">
			         <!-- <div class="files">
				        <input type="file" name="" >
				       </div> -->
				       <div class="input-group third-image-preview imgUploadView">
				             <img src="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" alt="<?=ADMIN_IMAGES_PATH.'/uploads_img.png'?>" class="third_default_order_img_upload">
			                <!-- <input type="text" class="form-control third-image-preview-filename" disabled="disabled"> --> <!-- don't give a name === doesn't send on POST/GET -->
			                <span class="input-group-btn">
			                    <!-- image-preview-clear button -->
			                    <button type="button" class="btn btn-default third-image-preview-clear" style="display:none;">
			                        <!-- <span class="glyphicon glyphicon-remove"></span> --> Clear
			                    </button>
			                    <!-- image-preview-input -->
			                    <div class="btn btn-default third-image-preview-input">
			                        <span class="title">Upload your image here</span>
			                        <span class="third-image-preview-input-title">Browse</span>
			                        <input type="file" accept="image/png, image/jpeg, image/gif" id="image_2"/> <!-- rename it -->

			                    </div>
			                </span>
			            </div><!-- /input-group image-preview [TO HERE]--> 
			      </div>
			    </div>
			  </div>
			</div>

			<div class="form-group m-t-25">
				<label for="orderName">Order Name</label>
					<div class="Name">
	                    <input type="text" 	placeholder="Enter your order name" id="order_name" class="form-control order_name" name="customer_order[order_name]">
	                                    <span class="text-danger" id="order_name_error"></span>
	                </div>
			</div>
			<div class="form-group ">
			   <div class="row">
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Delivery Date</label>
					<div class="Name">
	              	 	<div class="form-control-wrapper">
							<input type="text" name="customer_order[delievery_date]" id="deliverydate" class="form-control floating-label dateInput" placeholder="Enter Delivery Date" data-date-start-date="0d">
						</div>
	                </div>
			   	  </div>		   	  
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Item</label>
					<div class="Name">
	                    <select class="form-control" name="customer_order[parent_category_id]" id="parent_category_id">
	                    <?php foreach ($parent_category as $key =>$value){ ?>
	                    	<option value="<?=$value['name'];?>"><?=$value['name'];?></option>
	         			<?php } ?>
	                    </select>
	                     <span class="text-danger" id="parent_category_id_error"></span>
	                </div>
			   	  </div>
			   </div>
			</div>
			<div class="form-group ">
			   <div class="row">
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Length</label>
					<div class="Name">
	                    <input class="form-control" type="number" placeholder="Enter Length" value="" name="customer_order[length]" id="length">
	                     <span class="text-danger" id="length_error"></span>
	                </div>
			   	  </div>
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Length Type</label>
					<div class="Name">

	                   <select class="form-control" name="customer_order[length_type]" id="length_type">
	                    	<option value="Inches">Inches</option>
	                    	<option value="mm">mm</option>
	                    	<option value="cm">cm</option>                         
	                    </select>
	                     <span class="text-danger" id="length_type_error"></span>
	                </div>
			   	  </div>
			   </div>
			</div>
			<div class="form-group ">
			   <div class="row">
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Width</label>
					<div class="Name">
	                    <input class="form-control" type="number" placeholder="Enter Width" value="" name="customer_order[width]" id="width">
	                     <span class="text-danger" id="width_error"></span>
	                </div>
			   	  </div>
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Width Type</label>
					<div class="Name">
	                
	                    <select class="form-control" name="customer_order[width_type]" id="width_type">
	                    <option value="Inches">Inches</option>
	                    	<option value="mm">mm</option>
	                    	<option value="cm">cm</option>   
	                    </select>
	                     <span class="text-danger" id="width_type_error"></span>
	                </div>
			   	  </div>
			   </div>
			</div>
			<div class="form-group ">
			   <div class="row">
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Quantity</label>
					<div class="Name">
	                    <input class="form-control" type="number" placeholder="Enter Quantity" value="" name="customer_order[quantity]" id="quantity">
	                     <span class="text-danger" id="quantity_error"></span>
	                </div>
			   	  </div>
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Quantity Type</label>				
	                    <select class="form-control" id="quantity_type" name="customer_order[quantity_type]">
	                    	<option value="pieces">pieces</option>
	                    	<option value="pair">pair</option>	                    	    
	                    </select>
	                     <span class="text-danger" id="quantity_type_error"></span>
	                </div>
			   	  </div>
			 </div>
			
			<div class="form-group ">
			   <div class="row">
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Weight</label>
					<div class="Name">
	                    <input class="form-control" type="number" placeholder="Enter Weight" value=""  name="customer_order[weight_range_id]" id="weight_range_id">
	                     <span class="text-danger" id="weight_range_id_error"></span>
	                </div>
			   	  </div>
			   	  <div class="col-xs-6">
			   	  	<label for="orderName">Purity</label>
					<div class="Name">
	               
	                   <select class="form-control"  name="customer_order[purity]" id="purity">
	                    	<option value="22kt">22kt</option>
	                    	<option value="21kt">21kt</option>
	                    	<option value="18kt">18kt</option>    
	                    	<option value="other">other</option>    
	                    </select>
	                     <span class="text-danger" id="purity_error"></span>
	                </div>
			   	  </div>
			   </div>
			</div>
			<div class="form-group">
				<label for="orderName">Size</label>
					<div class="Name">
	                    <input class="form-control" type="number" placeholder="Enter Size" value="" name="customer_order[size]" id="size">
	                     <span class="text-danger" id="size_error"></span>
	                </div>
			</div>
			<div class="form-group">
				<label for="orderName">Remark</label>
					<div class="Name">
	                    <input class="form-control" type="text" placeholder="Enter Remark" value="" name="customer_order[remark]" id="remark">
	                     <span class="text-danger" id="remark_error"></span>
	                </div>
			</div>
			<div class="row btnFix">
				<div class="btnSubmit ">
				 	<button type="button" class="btn btn-primary helveticabold text-uppper"  id="creatOrder_btn" onclick="customer_order('store');" >Submit</button>
				</div>
			</div>
			<div id="snackbar"></div>
		</form>
	</div><!--frmLogin end-->
</div><!--container-fluid end--> 	
</body>
</html>