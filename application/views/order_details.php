<div class="container contaiBox">
 		<?php 
		 $order_date =date('Y-m-d', strtotime($order_details['order_date']. ' + 3 days'));	
		  $today = date('Y-m-d');	
		//echo (strtotime($today) ."<". strtotime($order_date));
	
		if(strtotime($today) < strtotime($order_date)){ 
			//echo"1";
			$displayNone="display:none";		
		}elseif ($order_details['co_status'] == "6") {
			//echo"2";
			$displayNone="display:none";
		}else{
			//echo"3";
			$displayNone="display:block";
		} ?>
			<div class="greybg fixedEdit" style="<?= $displayNone;?>">
			  <div class="clearfix borderradius8 text-right ">
				<div class="col-xs-12 p-r-0">
					<!-- <a href="<?=ADMIN_PATH?>order_edit/change_status/<?= $order_details['id']?>" onclick="cancel_order($order_details['id'])"><div class="btn btn-default borderradius8 borderOnly headingColor">Cancel Order</div></a> -->
					<a onclick="cancel_order('<?= $order_details['id']?>')" class="btn btn-default">Cancel Order</a>
				
					<a href="<?=ADMIN_PATH?>order_edit/<?= $order_details['id']?>" class="btn btn-default">Edit Order</a>
				</div>
			  </div>
			</div>


 		<div class="m-b-55 m-t-15">			
	 		<div class="greybg">
	 			<?php  ///print_r($order_details);?>
	 		    <div class="whitebg p15 clearfix borderradius8">
	 		    	<div class="row">
		 				<div class="col-md-8 col-sm-2  col-xs-4">
		 					<div class="productImg m-t-5 align-middle">
				 				<div class="main-slider proImg">
				 				<?php if(!empty($order_img_details[0]['img_file_path'])) { ?>
				 					<a class="fancybox" data-fancybox="gallery" rel="group" href="<?=SERVER_IMAGES.'/'.$order_details['image']?>">
										<img src="<?=SERVER_IMAGES.'/'.$order_img_details[0]['img_file_path']?>" class="img-responsive" />
									</a>
				 				<?php
				 							foreach (array_slice($order_img_details,1) as $key => $value) {
				 						?>
								  	<a class="fancybox" data-fancybox="gallery" rel="group" href="<?=SERVER_IMAGES.'/'.$value['img_file_path']?>">									
									</a>
				 							 					
				 				<?php }  } else { ?>
				 				<img src="<?=ADMIN_IMAGES_PATH.'/default_order_image.jpg'?>" alt="<?=ADMIN_IMAGES_PATH.'/default_order_image.jpg'?>" class="img-responsive">


				 				<?php } ?>
				 					
								</div>
		 					</div>
		 				</div>
		 				<div class="col-md-8 col-sm-10 col-xs-8 dashboardData">
		 				   <div class="row borderBottom m-b-5 p-b-5">
		 				   	   <div class="col-md-6 col-xs-6 helveticalight">Order No :</div>
		 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$order_details['id']?></div>
		 				   </div>
		 					<div class="row">
		 						<!-- <div class="col-xs-12 col-md-12 p-l-0 fontGreen m-b-5 p-b-5 borderBottom">Cancelled</div> -->
		 						
		 						<div class="col-md-6 col-xs-6 helveticalight">Order Date :</div>
		 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$order_details['order_date']?></div>
		 						<div class="col-md-6 col-xs-6 helveticalight">Delivery Date :</div>
		 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$order_details['delievery_date']?></div>
		 						<div class="col-md-6 col-xs-6 helveticalight">Qty :</div>
		 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$order_details['quantity']?></div>
		 						<div class="col-md-6 col-xs-6 helveticalight">Weight :</div>
		 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$order_details['weight_range_id']?></div>
		 						<div class="col-md-6 col-xs-6 helveticalight">Status :</div>
		 						<div class="col-md-6 col-xs-6 helveticaregular"><?=$order_details['status']?></div>
		 					</div>
		 				</div>
		 			</div>
	 			</div>
	 		</div>
	 		<div class="greybg m-t-15">
	 		    <div class="whitebg p15 clearfix borderradius8">
	 		    	<div class="row">
		 				<div class="col-xs-6 col-md-6 dashboardData">
	 						<div class="col-md-6 col-xs-6 p-l-0 m-b-5 helveticalight">Weight :</div>
	 						<div class="col-md-6 col-xs-6 p-l-0 helveticaregular m-b-5 "><?=blank_value($order_details['weight_range_id'])?></div>
	 						<div class="col-md-6 col-xs-6 p-l-0 m-b-5 helveticalight">Purity :</div>
	 						<div class="col-md-6 col-xs-6 p-l-0 helveticaregular m-b-5 "><?=blank_value($order_details['purity'])?></div>
	 						<div class="col-md-6 col-xs-6 p-l-0 m-b-5 helveticalight">Length :</div>
	 						<div class="col-md-6 col-xs-6 p-l-0 helveticaregular m-b-5 "><?php if(!empty($order_details['length'])){ echo $order_details['length'].' '.$order_details['length_type']; }else{ echo "-"; } ?></div>
	 						<div class="col-md-6 col-xs-6 p-l-0 m-b-5 helveticalight">Remark :</div>
	 						<div class="col-md-6 col-xs-6 p-l-0 helveticaregular m-b-5 "><?=blank_value($order_details['remark'])?></div>	 					
		 				</div>
		 				<div class="col-xs-6 col-md-6 dashboardData">
		 					<div class="row">
		 						<div class="col-md-6 col-xs-6 p-l-0 m-b-5 helveticalight">Size :</div>
		 						<div class="col-md-6 col-xs-6 p-l-0 helveticaregular m-b-5 "><?=blank_value($order_details['size'])?></div>
		 						<div class="col-md-6 col-xs-6 p-l-0 m-b-5 helveticalight">Quantity :</div>
		 						<div class="col-md-6 col-xs-6 p-l-0 helveticaregular m-b-5 "><?=blank_value($order_details['quantity'])?></div>
		 						<div class="col-md-6 col-xs-6 p-l-0 m-b-5 helveticalight">Width:</div>
		 						<div class="col-md-6 col-xs-6 p-l-0 helveticaregular m-b-5 "><?php if(!empty($order_details['width'])){ echo $order_details['width'].' '.$order_details['width_type']; }else{ echo "-"; }?></div>
		 					</div>
		 				</div>
	 				</div>
	 			</div>
	 		</div>
	 		<?php if($order_details['order_history']=='1'){?>
	 		<div class="row greybg">
	 		 	 <div class="col-xs-12 text-center">
	 		  	 	<a href="<?=ADMIN_PATH?>view_change_history/<?= $order_details['id']?>" class="p-t-b-5 d-block headingColor">View Change History</a>
	 		  	 </div>
	 		</div>
	 		<?php } 
	 		 if($order_details['co_status'] !="6"){ ?>
	 		<div class="greybg mtb15 orderStatus">
	 			<div class="col-xs-12 whitebg min-height97">
	 				<div class="row p-all5 fontSize12 p-b-0 ">
	 					<div class="col-xs-4 m-t-15">
	 						<p>Dispatched</p>
	 					</div>
	 					<div class="col-xs-4 m-t-15">
	 					<?php  if(!empty($order_details['dispatch_date'])){ 
	 									$dispatch_date= $order_details['dispatch_date']; 
	 									$dispatch_img="Delivered-2.png";
	 							}else{ 
	 									$dispatch_date= $order_details['delievery_date']; 
	 									$dispatch_img="Delivered-1.png";
	 							} 
	 					?>
	 						<div class="circle text-center first-step"><img src="<?=ADMIN_IMAGES_PATH?>/<?=$dispatch_img?>" alt="<?= $dispatch_img;?>" class="m-t-20"></div>
	 			
	 					</div>
	 					<div class="col-xs-4 m-t-15"><p class="helveticalight">Estimated Delivery Date  
	 					<?= blank_value($dispatch_date);?></p></div>
	 				</div>
	 			</div>
	 			<div class="col-xs-12 whitebg">
	 				<div class="row p-all5 fontSize12 p-t-0">
	 					<div class="col-xs-4 ">
	 						
	 					</div>
	 					<div class="col-xs-4">
	 						<div class="text-center"><span class="vertlicalLing">&nbsp;</span></div>
	 					</div>
	 					<div class="col-xs-4 "></div>
	 				</div>
	 			</div>
	 		<!-- </div>
	 		<div class="row greybg p-all5"> -->
 			<div class="col-xs-12 whitebg ">
 				<div class="row p-all5 fontSize12 min-height">
 					<div class="col-xs-4 m-t-15">
 						<p>Ready for delivery</p>
 						<p class="helveticalight">Your order is ready</p>
 					</div>
 					<div class="col-xs-4 m-t-15">
 						<?php  if(!empty($order_details['ready_for_delievery_date'])){ 
 									
 									$rdyf_del_dt_img="Ready-for-delivery-2.png";
 							}else{ 
 									
 									$rdyf_del_dt_img="Ready-for-delivery-1.png";
 							} 
 					?>
 		
 						<div class="circle text-center"><img src="<?=ADMIN_IMAGES_PATH?>/<?=
 						 $rdyf_del_dt_img;?>" alt="<?= $rdyf_del_dt_img;?>" class="m-t-20"></div>
 					</div>
 					<div class="col-xs-4 m-t-15"><p class="helveticalight">Date  <?= blank_value($order_details['ready_for_delievery_date']
 					)?></p></div>
 				</div>
 			</div>
 			<div class="col-xs-12 whitebg ">
 				<div class="row p-all5 fontSize12 min-height">
 					<div class="col-xs-4 m-t-15">
 						<p>In Production</p>
 						<p class="helveticalight">We are preparing your order</p>
 					</div>
 					<?php  if(!empty($order_details['inproduction_date']) && $order_details['co_status']!='5'){ 
 									
 									$inproduction_date_img="In-Production-2.png";
 							}else{ 
 									
 									$inproduction_date_img="In-Production-1.png";
 							} 
 					?>
 					<div class="col-xs-4 m-t-15">
 						<div class="circle text-center"><img src="<?=ADMIN_IMAGES_PATH?>/<?= $inproduction_date_img?>" alt="<?= $inproduction_date_img?>" class="m-t-20"></div>
 					
 					</div>
 					<div class="col-xs-4 m-t-15"><p class="helveticalight">Date  <?=blank_value($order_details['inproduction_date'])?></p></div>
 				</div>
 			</div>
	 		<!-- </div>
	 		<div class="row greybg p-all5"> -->
	 			<div class="col-xs-12 whitebg ">
	 				<div class="row p-all5 fontSize12 min-height">
	 					<div class="col-xs-4 m-t-15">
	 						<p>Pending</p>
	 						<p class="helveticalight">Your order is on hold</p>
	 					</div>
	 					<?php  if(!empty($order_details['pending_date']) || $order_details['co_status']=='5'){ 
	 									
	 									$pending_date_img="Pending-2.png";
	 							}else{ 
	 									
	 									$pending_date_img="Pending-1.png";
	 							} 
	 					?>
	 					<div class="col-xs-4 m-t-15">
	 						<div class="circle text-center"><img src="<?=ADMIN_IMAGES_PATH?>/<?=$pending_date_img;?>" alt="<?=$pending_date_img;?>" class="m-t-20"></div>
	 					
	 					</div>
	 					<div class="col-xs-4 m-t-15"><p class="helveticalight">Date  <?=blank_value($order_details['pending_date'])?></p></div>
	 				</div>
	 			</div>
	 		<!-- </div>
	 		<div class="row greybg p-all5"> -->
	 			<div class="col-xs-12 whitebg ">
	 				<div class="row p-all5 fontSize12 min-height">
	 					<div class="col-xs-4 m-t-15">
	 						<p>Order Placed</p>
	 						<p class="helveticalight">We have received your order</p>
	 					</div>
	 					<div class="col-xs-4 m-t-15">
	 						<div class="circle text-center"><img src="<?=ADMIN_IMAGES_PATH?>/Cart-2.png" alt="Delivered-1" class="m-t-20"></div>
	 						<div class="circle text-center displayNone"><img src="<?=ADMIN_IMAGES_PATH?>/Cart-1.png" alt="Delivered-2" class="m-t-20"></div>
	 					</div>
	 					<div class="col-xs-4 m-t-15"><p class="helveticalight">Date  <?=blank_value($order_details['order_date'])?></p></div>
	 				</div>
	 			</div>
	 		</div>
	 		
	 	<?php }else{ ?>
	 		<div class=""><center class="cancelledOrd"> Your order cancelled. </center></div>
	 	<?php } ?>
 	</div><!--frmLogin end-->

		
 	</div><!--container-fluid end-->



</body>
</html>