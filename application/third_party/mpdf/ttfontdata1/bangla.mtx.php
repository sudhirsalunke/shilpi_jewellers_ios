<?php
$name='Bangla';
$type='TTF';
$desc=array (
  'CapHeight' => 863,
  'XHeight' => 0,
  'FontBBox' => '[-670 -265 905 863]',
  'Flags' => 4,
  'Ascent' => 863,
  'Descent' => -233,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 109,
  'MissingWidth' => 433,
);
$unitsPerEm=2048;
$up=-106;
$ut=78;
$strp=259;
$strs=50;
$ttffile='/var/www/html/shilpi_jewellers/application/third_party/mpdf/ttfonts/Bangla.ttf';
$TTCfontID='0';
$originalsize=140340;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='bangla';
$panose=' 0 0 3 0 6 3 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 863, -265, 0
// usWinAscent/usWinDescent = 863, -233
// hhea Ascent/Descent/LineGap = 863, -265, 90
$useOTL=0x0000;
$rtlPUAstr='';
?>