<?php
$name='Kalpurush';
$type='TTF';
$desc=array (
  'CapHeight' => 615,
  'XHeight' => 454,
  'FontBBox' => '[-555 -402 1118 1033]',
  'Flags' => 4,
  'Ascent' => 1000,
  'Descent' => -400,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 672,
);
$unitsPerEm=1000;
$up=-307;
$ut=20;
$strp=250;
$strs=50;
$ttffile='/var/www/html/shilpi_jewellers/application/third_party/mpdf/ttfonts/Kalpurush.ttf';
$TTCfontID='0';
$originalsize=314152;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='kalpurush';
$panose=' 0 0 2 0 6 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 1000, -400, 259
// usWinAscent/usWinDescent = 1000, -400
// hhea Ascent/Descent/LineGap = 1000, -400, 259
$useOTL=0x0000;
$rtlPUAstr='';
?>