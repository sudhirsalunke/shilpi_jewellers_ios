<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation{

    function verifyCredentials() {   
        $ci = & get_instance();
        $ci->db->select('*');
        $ci->db->where('email', $ci->input->post('username'));
        //$ci->db->where('encrypted_password', MD5($ci->input->post('password')));
        $ci->db->where('encrypted_password', $ci->input->post('password'));
        $query = $ci->db->get('customer')->row_array();
        if (count($query) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function verifyEmail() {
        $ci = & get_instance();
        $ci->db->select('email');
        $ci->db->where('email', $ci->input->post('email'));
        $query = $ci->db->get('login')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }

    function verifyIdentifier() {
        $ci = & get_instance();
        $ci->db->select('unique_identifier');
        $ci->db->where('unique_identifier', $ci->input->post('unique_identifier'));
        $query = $ci->db->get('login')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function password_check($str) {
        if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
            return TRUE;
        }
        return FALSE;
    }

    

    public function check_pc_wr(){
    $ci = & get_instance();
    $all_data = $ci->input->post('product');
      if (empty($_POST['check_array'])) {
           $_POST['check_array']=array();
      }
      if (in_array(array($all_data['parent_category'],$all_data['weight']), $_POST['check_array'])) {
          return FALSE;
      }else{
        $_POST['check_array'][]=array($all_data['parent_category'],$all_data['weight']);
        return true;
      }

    }

    public function check_customer_email_exists(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_customer_email_exists', 'Email Id already exists');
        $all_data = $ci->input->post();
        $ci->db->select('*');
        $ci->db->from('customer');
        $ci->db->where('email',$all_data['email']);
        $ci->db->where('customer_type_id','2');
        if(!empty($all_data['customer_id']))
            $ci->db->where('id !=',$all_data['customer_id']);
        $result = $ci->db->get()->row_array();
        if(!empty($result)){
            return false;
        }else{
            return true;
        }
    }

    public function checkFORGOTEMAIL(){
        $ci = & get_instance();
        $postData=$ci->input->post();
        $ci->db->select('*');
        $ci->db->where('email',$postData['email']);
        $query = $ci->db->get('customer')->row_array();
        if (count($query) > 0) {
            return TRUE;
        } else {
            return FALSE;
        } 
    }

}//class