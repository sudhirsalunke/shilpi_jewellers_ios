<?php
class User_model extends CI_Model {

    function __construct() {
        $this->table_name = "login";
        parent::__construct();
    }


  public function validateAdminLogin(){
        $this->load->config('admin_validationrules', TRUE);
        $this->form_validation->set_message('verifyCredentials', 'Invalid Email Or Password');
        $this->form_validation->set_rules($this->config->item('login', 'admin_validationrules'));
        $data['status']= 'success';
        if($this->form_validation->run()===FALSE){
          $data['status']= 'failure';
          $data['data']= '';
          $data['error'] = $this->getErrorMsg();
        }
        return $data;
   }
  private function getErrorMsg(){
    return array(
        'username'=>strip_tags(form_error('username')),
        'password'=>strip_tags(form_error('password')),
      );
    }

 
  public function get_images_by_order_id($order_id){
            $this->db->select('img_file_path');
     return $this->db->get_where('customer_orders_img',array('customer_id'=>$order_id))->result_array();
  }

  public function get_by_user_id($customer_id){
            $this->db->select('*');
     return $this->db->get_where('customer',array('id'=>$customer_id))->row_array();
  }


  public function get_order_by_user_id($customer_id){  
      $this->db->select('co.*,DATE_FORMAT(co.order_date,"%d/%m/%Y") AS order_date,ifnull(DATE_FORMAT(co.change_delivery_date,"%d/%m/%Y"),DATE_FORMAT(co.delievery_date,"%d/%m/%Y")) AS delievery_date,DATE_FORMAT(co.expected_date,"%d/%m/%Y") AS expected_date,DATE_FORMAT(co.sent_to_karigar_date,"%d/%m/%Y") AS sent_to_karigar_date,DATE_FORMAT(co.pending_date,"%d/%m/%Y") AS pending_date,DATE_FORMAT(co.inproduction_date,"%d/%m/%Y") AS inproduction_date,DATE_FORMAT(co.receive_date,"%d/%m/%Y") AS ready_for_delievery_date,DATE_FORMAT(co.actual_delivery_date,"%d/%m/%Y") AS dispatch_date,(CASE co.status WHEN "0"  THEN "Order Placed" WHEN "1" THEN "Pending" WHEN "2" THEN "In production" WHEN "3" THEN "Ready for delivery" WHEN "4" THEN "Dispatched" WHEN "5" THEN "Pending" WHEN "6" THEN "Cancelled" END) as status');
      $this->db->from('customer_orders co');
      $this->db->where('co.customer_id',$customer_id);
      $this->db->where('added_by','app');
      $this->db->order_by('co.id','DESC');
      $result = $this->db->get()->result_array();
      foreach ($result as $key => $value) {
        $result[$key]['images']=$this->get_images_by_order_id($value['id']);
      }

      return $result;
  }

  public function update(){
      $postdata = $_POST;
      //print_r($_FILES);die;
      $update_array = array(
          'name' => $postdata['name'],
          'email' => $postdata['email'],
          'updated_at' => date('Y-m-d H:i:s')
          );
      $this->db->where('id',$_POST['user_id']);
      if($this->db->update('customer',$update_array)){
          $this->update_upload_user_image($postdata,$_POST['user_id']); 
          return $data['status']="success";
      }else{
          return $data['status']="error";
      }
  }
    
  private function update_upload_user_image($postdata,$id)
  {  
   $imagepath="";
    if(!empty($postdata['slim'])){
            $file = upload_slim_image('slim',$folder_name="user_profile");
            if (!empty($file['path'])) {
                $uploadData['imagepath'] =$imagepath=$file['path'];
                $uploadData['updated_at']=date('Y-m-d H:i:s');
                  $this->db->where('id',$id);
                  $this->db->update('customer',$uploadData);
            }  
    }        
     return $imagepath;
} 
    public function validateUserProfile(){
        $this->load->config('admin_validationrules', TRUE);
        $this->form_validation->set_rules($this->config->item('UserProfile', 'admin_validationrules'));
        $data['status']= 'success';
        if($this->form_validation->run()===FALSE){
          $data['status']= 'failure';
          $data['data']= '';
          $data['error'] = $this->getUserErrorMsg();
        }
        return $data;
    }

    private function getUserErrorMsg(){
        return array(
          'name'=>strip_tags(form_error('name')),
          'email'=>strip_tags(form_error('email')),
          'mobile'=>strip_tags(form_error('mobile')),
          'company'=>strip_tags(form_error('company')),
          'password'=>strip_tags(form_error('password')),
        );
    }  
    public function create_user_info(){
        $postdata=$this->input->post();
        $insert_array = array(
          'client_code' => 'c'.mt_rand(),
          'name' => $postdata['name'],
          'email' => $postdata['email'],
          'mobile_no' => $postdata['mobile'],
          'company_name' => $postdata['company'],
          'encrypted_password' => $postdata['password'],
          'added_by' => 'app',
          'customer_type_id' => '2',
          'encrypted_id'=>$this->data_encryption->get_encrypted_id(),
          'created_at' => date('Y-m-d H:i:s')
        );
        //print_r($insert_array);
        if($this->db->insert('customer',$insert_array)){
              $data['status']= 'success';
              $data['data']= '';
              $data['error'] = "";
              return $data;
              
          }else{
              $data['status']= 'failure';
              $data['data']= '';
              $data['error'] = 'Something Went Wrong Try Again';
              return $data;
          }
    }
     public function check_password_by_id($data){
       $this->db->select('encrypted_password');
     return $this->db->get_where('customer',array('id'=>$data['user_id'],'encrypted_password'=>$data['old_password']))->row_array();     
       
    } 

    public function update_password_by_id($data){
        $updated_data['encrypted_password']=$data['new_password'];
        $updated_data['updated_at'] = date("Y-m-d H:i:s");
         $this->db->where('id',$data['user_id']);
         $res = $this->db->update('customer',$updated_data);
        if($res){
            return TRUE; 
        }
       
    }
  public  function validateForgotPassswordData() 
    {
        $this->form_validation->set_rules($this->config->item('forgot_password', 'admin_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }  
  public  function forgot_password($email,$data){
     $result = $this->db->update('customer',$data,array('email'=>$email));
     $res=$this->password_link_send_email($email,$data);
      return TRUE;
  }

 private function password_link_send_email($email,$linkdata){
        $user = get_user_by_email($email);
       
        $data['name']= $user['name'];
        $data['encrypted_password']= $linkdata['encrypted_password'];
       // print_r($data);exit;
        $mail['to'] = $email;
        $from="roopsingh@shilpijewels.com";
        $from_name="Shilpi_jewlwers";
        //print_r($email);
        $result =$this->email_lib->send($data,$email,'Forgot Password','email_template/forgot_password_email.php',$from,$from_name);
        //print_r($result);
        return $result;
    }

}
?>