<?php
class Order_model extends CI_Model {

    function __construct() {
        $this->table_name = "customer_orders";
        parent::__construct();
    }
    public function validatepostdata(){
        //print_r($_POST['customer_order']);exit;
      if($_POST['customer_order']['purity'] != 'other'){
            $this->form_validation->set_rules($this->config->item('customer_order','admin_validationrules'));
        }else{
            $this->form_validation->set_rules($this->config->item('customer_order_remark','admin_validationrules'));
        }
     if ($this->form_validation->run() == FALSE) {        
          return FALSE;
      } else {
          return TRUE;
      }
  }



    public function get_parent_category(){
      $this->db->select('id,name');
      $this->db->from('parent_category');
      $this->db->where('app_order',1);
      $this->db->order_by('name','asce');
      $result = $this->db->get()->result_array();
      return $result;
    }

   public function store($post_array=""){
        if(!empty($post_array)){
        $postdata = $this->input->post('customer_order');
        //print_r($postdata);die;
        $insert_data=$this->form_data($postdata);     
      if ($this->db->insert('customer_orders',$insert_data)) {                 
          $response=get_successMsg();
          $response['order_status']=0;
          $response['order_id']=$this->db->insert_id(); 
          if($_FILES != ""){
            $this->upload_image($postdata); 
          }       
          return $response;
        }else{
          return get_errorMsg();
        }
    }
}

    private function form_data($postdata)
      { 
        //print_r($postdata);die;
        $postdata['created_at']=date('Y-m-d H:i:s');
        $postdata['updated_at']=date('Y-m-d H:i:s');
        $postdata['delievery_date']=date('Y-m-d',strtotime($postdata['delievery_date']));
        $postdata['expected_date']=date('Y-m-d', strtotime($postdata['delievery_date']. ' - 3 days'));
        $postdata['order_date']=date('Y-m-d');
        $postdata['added_by']='app';
        $postdata['status']=0;
        $postdata['create_order_mail']=0;
        $postdata['remaining_qty']=$postdata['quantity'];
        $postdata['quantity']=$postdata['quantity'];
        $postdata['quantity_type']=$postdata['quantity_type'];
        $postdata['customer_id']=$_SESSION['user_id'];
        $postdata['company_name']=$_SESSION['company_name'];
        $postdata['party_name']=$_SESSION['name'];
        $postdata['mobile_no']=$_SESSION['mobile_no'];
        $postdata['email_id']=$_SESSION['email'];
        $postdata['prepared_by']=$_SESSION['user_id'];
        return $postdata;
      } 

    private function upload_image($postdata)
    {   

          if(!empty($_FILES['image']['name'][0])){   
          { //print_r($_FILES['image']);exit;     
            foreach ($_FILES['image']['name'] as $key => $val )
            //print_r($upload_img[$key]['file_name']);
                if(!empty($val)){
                $val1 = true;
                $file_array['name'] = $val;
                $file_array['type'] = $_FILES['image']['type'][$key];
                $file_array['tmp_name'] = $_FILES['image']['tmp_name'][$key];
                $file_array['error'] = $_FILES['image']['error'][$key];
                $file_array['size'] = $_FILES['image']['size'][$key];
                $upload_img[$key] =store_with_compress_image('order_module',$file_array);
                //print_r($upload_img);die('asd');
                $name_temp = $upload_img[$key];
                //print_r($name_temp);
                $uploadData[$key]['img_file_path'] =$name_temp;
                $uploadData[$key]['created_at'] = date("Y-m-d H:i:s");
                $uploadData[$key]['updated_at'] = date("Y-m-d H:i:s");         
                $uploadData[$key]['customer_id'] = $this->db->insert_id();
              }
          }//die;
        }

        if(isset($val1) && $val1 == true){
          $this->db->insert_batch('customer_orders_img',$uploadData);
        }

    }  
 


    public function get_images($order_id){
        $this->db->select('id,img_file_path');
        $this->db->from('customer_orders_img');
        $this->db->where('customer_id',$order_id);
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function find($order_id){
       $this->db->select('co.*,d.name as department_name,a.username as prepared_by,co.weight_range_id as approx_weight,km.name as karigar_name,c.name category_name,cor.karigar_rcpt_voucher_no,km.email as k_email');
        $this->db->from($this->table_name.' co');
        $this->db->join('category_master c','co.category_id=c.id','left');
        $this->db->join('customer_order_received cor','cor.order_id=co.id','left');
        $this->db->join('departments d','co.department_id=d.id','left');
        $this->db->join('karigar_master km','co.karigar_id=km.id','left');
        $this->db->join('admin a','co.prepared_by=a.id','left');
        $this->db->where('co.id',$order_id);
      
        return $this->db->get()->row_array();

    }

    
    public function get_order($order_id){  
        $this->db->select('co.*,coi.img_file_path as image,DATE_FORMAT(co.order_date,"%d/%m/%Y") AS order_date,ifnull(DATE_FORMAT(co.change_delivery_date,"%d/%m/%Y"),DATE_FORMAT(co.delievery_date,"%d/%m/%Y")) AS delievery_date,DATE_FORMAT(co.expected_date,"%d/%m/%Y") AS expected_date,DATE_FORMAT(co.sent_to_karigar_date,"%d/%m/%Y") AS sent_to_karigar_date,DATE_FORMAT(co.pending_date,"%d/%m/%Y") AS pending_date,DATE_FORMAT(co.inproduction_date,"%d/%m/%Y") AS inproduction_date,DATE_FORMAT(co.receive_date,"%d/%m/%Y") AS ready_for_delievery_date,DATE_FORMAT(co.actual_delivery_date,"%d/%m/%Y") AS dispatch_date,(CASE co.status WHEN "0"  THEN "Order Placed" WHEN "1" THEN "Pending" WHEN "2" THEN "In production" WHEN "3" THEN "Ready for delivery" WHEN "4" THEN "Dispatched" WHEN "5" THEN "Pending" WHEN "6" THEN "Cancelled" END) as status,co.status as co_status,DATE_FORMAT(co.delievery_date,"%d-%m-%Y") AS delievery,co.created_at');
        $this->db->from('customer_orders co');      
        $this->db->join('customer_orders_img coi','coi.customer_id = co.id','left');
        $this->db->where('co.id',$order_id);
        $this->db->where('added_by','app');
        $result = $this->db->get()->row_array(); 
        return $result;
    } 

    public function get_images_by_order_id($order_id){
              $this->db->select('id,img_file_path');
       return $this->db->get_where('customer_orders_img',array('customer_id'=>$order_id))->result_array();
    } 
    public function update_order(){ 
        $updated_data = $this->input->post('customer_order');
      // print_r($_FILES);die;
         //  echo "<pre>";print_r($updated_data['delievery_date']);echo "</pre>";
         // print_r(date('Y-m-d',strtotime($updated_data['delievery_date'])));die;
          if($updated_data['status'] == 2){
           $updated_data['status'] = 5;
         }
         
          $updated_data['delievery_date']=date('Y-m-d',strtotime($updated_data['delievery_date']));
          $updated_data['expected_date']=date('d-m-Y', strtotime($updated_data['delievery_date']. ' - 3 days'));
        
          $select = array_keys($updated_data);
          $before_update = $this->find_old_order_data($updated_data['id']);  
          foreach ($before_update as $col_key => $col_value) {
            if (!in_array($col_key, $select)) {
              unset($before_update[$col_key]);
            }
          }
          $order_result=array_diff_assoc($before_update,$updated_data);
            //   echo "<pre>-----------post";
            //         print_r($updated_data);
            // echo "</pre>";
            //  echo "<pre>-----------query";
            //         print_r($before_update);
            // echo "</pre>";
            //  echo "<pre>-----------result";
            //   print_r($order_result);die();

          $updated_data['order_history'] = '1';
          $updated_data['updated_at'] = date('Y-m-d H:i:s'); 
          $updated_data['remaining_qty']=$updated_data['quantity'];
           $updated_data['expected_date']=date('Y-m-d', strtotime($updated_data['expected_date']));

          $order_result['added_by']='app';
          $order_result['order_id']=$updated_data['id'];
          //print_r( $order_result);die;
          $order_history_value=order_history_update($order_result);
         $this->db->where('id',$updated_data['id']);
         $res = $this->db->update('customer_orders',$updated_data);
         $this->update_upload_image($updated_data,$updated_data['id']); 
          if($res){
              
                return true;
            }else{
                return get_errorMsg();
            }
    }

     private function update_upload_image($postdata,$id){   
       //print_r($_FILES);die;
      if(!empty($_FILES)){
        $updated_ids = @$_POST['image_ids'];
         $update_array=$insert_array=array();

            foreach ($_FILES['image']['name'] as $key => $value) {   
            //print_r($value); 
              if (!empty($value)) {
          
                $file_array['name'] = $value;
                $file_array['type'] = $_FILES['image']['type'][$key];
                $file_array['tmp_name'] = $_FILES['image']['tmp_name'][$key];
                $file_array['error'] = $_FILES['image']['error'][$key];
                $file_array['size'] = $_FILES['image']['size'][$key];

                $upload_img =store_with_compress_image('order_module',$file_array);
                //print_r($upload_img);die();
               // print_r($upload_img);
                $name_temp = $upload_img;
                $uploadData=array();
                $uploadData['img_file_path'] =$name_temp;
                $uploadData['created_at'] = date("Y-m-d H:i:s");
                $uploadData['updated_at'] = date("Y-m-d H:i:s");         
                $uploadData['customer_id'] = $id;

                //print_r($updated_ids[$key]);
                  if (!empty($updated_ids[$key])) {
                    $uploadData['id']=$updated_ids[$key];
                    $update_array[]=$uploadData;
                  }else{
                    $insert_array[]=$uploadData;
                  }

               }
             
            } 
            // print_r($updated_ids);
            // echo"<pre> insert";
            // print_r($insert_array);
            // echo"</pre> ---------------------------------";

            // echo"<pre> update";
            // print_r($update_array);
            // echo"</pre>";
            /*die;*/
            if(!empty($insert_array)){
              $this->db->insert_batch('customer_orders_img',$insert_array);
             }

            if(!empty($update_array)){
              $this->db->update_batch('customer_orders_img',$update_array,'id');
             }

        }     

    }  
    public function find_old_order_data($order_id){

       $this->db->select('co.*,d.name as department_name,a.username as prepared_by,co.weight_range_id as approx_weight,km.name as karigar_name,c.name category_name,cor.karigar_rcpt_voucher_no,km.email as k_email,DATE_FORMAT(co.order_date,"%d-%m-%Y")  order_date , DATE_FORMAT(ifnull(co.change_delivery_date,co.delievery_date),"%d-%m-%Y") delivery_date, DATE_FORMAT(co.expected_date,"%d-%m-%Y")  expected_date ');
                $this->db->from('customer_orders co');
        $this->db->join('category_master c','co.category_id=c.id','left');
        $this->db->join('customer_order_received cor','cor.order_id=co.id','left');
        $this->db->join('departments d','co.department_id=d.id','left');
        $this->db->join('karigar_master km','co.karigar_id=km.id','left');
        //$this->db->join('weights w','co.weight_range_id=w.id');
        $this->db->join('admin a','co.prepared_by=a.id','left');
        $this->db->where('co.id',$order_id);
      
        return $this->db->get()->row_array();

   }



  public function  get_order_history($order_id){
        $this->db->select('REPLACE(filed_name, "_", " ") as filed_name,filed_value, DATE_FORMAT( order_change_date,  "%d-%M-%Y  %T" ) as order_change_date');
        $this->db->from('customer_orders_history'); 
        $this->db->where('order_id',$order_id);
        //$this->db->group_by('order_change_date');
       $result = $this->db->get()->result_array();
       return filter_reponse_array($result,'order_change_date');
   // print_r($order_id);die;


  }

    public function change_status($order_id){
        $updated_data['status']=6;
        $updated_data['updated_at'] = date("Y-m-d H:i:s");
         $this->db->where('id',$order_id);
         $res = $this->db->update('customer_orders',$updated_data);
        if($res){
            return TRUE; 
        }   
    }


  
}
?>